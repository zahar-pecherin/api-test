class AddPremiumToDeals < ActiveRecord::Migration[5.0]
  def change
    add_column :deals, :premium, :boolean, default: false
  end
end
