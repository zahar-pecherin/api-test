class AddCitiesMigration < ActiveRecord::Migration[5.0]
  def up
    create_table :cities do |t|
      t.text :name
      t.boolean :status, :default => true
      t.timestamps null: false
    end
  end

  def down
    drop_table :cities
  end
end
