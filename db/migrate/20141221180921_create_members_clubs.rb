class CreateMembersClubs < ActiveRecord::Migration[5.0]
  def change
    create_table :members_clubs do |t|
      t.references :club, index: true
      t.references :member, index: true

      t.timestamps null: false
    end
    add_foreign_key :members_clubs, :clubs
    add_foreign_key :members_clubs, :members
    add_index :members_clubs, [:member_id, :club_id], unique: true
  end
end
