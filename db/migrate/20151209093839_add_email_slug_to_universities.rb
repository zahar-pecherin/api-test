class AddEmailSlugToUniversities < ActiveRecord::Migration[5.0]
  def change
    add_column :universities, :email_slug, :string
  end
end
