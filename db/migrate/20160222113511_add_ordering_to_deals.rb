class AddOrderingToDeals < ActiveRecord::Migration[5.0]
  def change
    add_column :deals, :ordering, :integer, :default => 0
  end
end
