class CreateClubMerchants < ActiveRecord::Migration[5.0]
  def change
    create_table :club_merchants do |t|
      t.string :discount
      t.text :deal
      t.references :club, index: true
      t.references :merchant, index: true

      t.timestamps null: false
    end
    add_foreign_key :club_merchants, :clubs
    add_foreign_key :club_merchants, :merchants
    add_index :club_merchants, [:merchant_id, :club_id], unique: true
  end
end
