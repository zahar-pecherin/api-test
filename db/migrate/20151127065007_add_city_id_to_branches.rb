class AddCityIdToBranches < ActiveRecord::Migration[5.0]
  def change
    add_column :branches, :city_id, :integer
  end
end
