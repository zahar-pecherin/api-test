class AddMediaToMember < ActiveRecord::Migration[5.0]
  def change
    add_reference :members, :media, index: true
    add_foreign_key :members, :media, column: :media_id
    remove_column :members, :profile_image, :string
  end
end
