class AddColsToMembership < ActiveRecord::Migration[5.0]
  def change
    add_column :memberships, :referrals, :integer
    add_column :memberships, :referred_by, :integer
    add_column :memberships, :signup_source, :string
  end
end
