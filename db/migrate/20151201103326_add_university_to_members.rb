class AddUniversityToMembers < ActiveRecord::Migration[5.0]
  def change
    add_reference :members, :university, index: true
  end
end
