class AddAccountTypeToBankingDetails < ActiveRecord::Migration[5.0]
  def change
    add_column :banking_details, :account_type, :integer
  end
end
