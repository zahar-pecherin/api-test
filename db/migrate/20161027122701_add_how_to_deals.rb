class AddHowToDeals < ActiveRecord::Migration[5.0]
  def change
    add_column :merchants, :how_to_redeem, :text
  end
end
