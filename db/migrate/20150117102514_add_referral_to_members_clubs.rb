class AddReferralToMembersClubs < ActiveRecord::Migration[5.0]
  def change
    add_column :members_clubs, :referral_code, :string
  end
end
