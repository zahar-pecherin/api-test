class CreateMemberFavourites < ActiveRecord::Migration[5.0]
  def change
    create_table :member_favourites do |t|
      t.references :member, index: true
      t.references :deal, index: true

      t.timestamps null: false
    end
    add_foreign_key :member_favourites, :members
    add_foreign_key :member_favourites, :deals
  end
end
