class AddUrlToMerchants < ActiveRecord::Migration[5.0]
  def change
    add_column :merchants, :url, :string
  end
end
