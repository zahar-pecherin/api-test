class RemoveMemberClubsIdFromPayments < ActiveRecord::Migration[5.0]
  def change
    remove_reference :payments, :members_clubs, index: true
    #remove_foreign_key :payments, :members_clubs
  end
end
