class AddColsToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :study_region, :string
    add_column :members, :postal_address, :text
    add_column :members, :postal_city, :string
    add_column :members, :postal_province, :string
    add_column :members, :postal_code, :string
    add_column :members, :mobile_number, :string
    remove_column :members, :facebook_id, :string
    remove_column :members, :twitter_id, :string
  end
end
