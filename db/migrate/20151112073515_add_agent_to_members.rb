class AddAgentToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :is_agent, :boolean, :default => false
  end
end
