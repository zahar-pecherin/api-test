class AddRessetPasswordTokenToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :reset_password_token, :string
    add_column :members, :reset_password_token_sent_at, :datetime
  end
end
