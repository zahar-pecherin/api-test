class AddColsToMember < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :twitter_id, :string
    add_column :members, :facebook_id, :string
  end
end
