class AddSeoToDeals < ActiveRecord::Migration[5.0]
  def change
    add_column :deals, :seo_keyword, :text
    add_column :deals, :seo_description, :text
  end
end
