class AddTermsToMerchants < ActiveRecord::Migration[5.0]
  def change
    add_column :merchants, :terms, :text
  end
end
