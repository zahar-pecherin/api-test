class AddBranchesMigration < ActiveRecord::Migration[5.0]
  def up
    create_table :branches do |t|
      t.belongs_to :merchant, index: true
      t.text :address
      t.text :phone
      t.text :email
      t.decimal :lng
      t.decimal :lat
      t.timestamps null: false
    end

  end

  def down
    drop_table :branches
  end
end
