class AddReferralExportedToMemberships < ActiveRecord::Migration[5.0]
  def change
    add_column :memberships, :processed_for_referral, :boolean, null: false, default: false
  end
end
