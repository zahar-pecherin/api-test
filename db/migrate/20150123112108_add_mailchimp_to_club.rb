class AddMailchimpToClub < ActiveRecord::Migration[5.0]
  def change
    add_column :clubs, :mailchimp_list, :string
  end
end
