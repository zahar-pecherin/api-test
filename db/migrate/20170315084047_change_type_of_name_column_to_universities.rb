class ChangeTypeOfNameColumnToUniversities < ActiveRecord::Migration[5.0]
  def change
    change_column :universities, :name, :string
  end
end
