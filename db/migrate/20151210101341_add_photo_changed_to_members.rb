class AddPhotoChangedToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :photo_changed, :boolean, :default => false
  end
end
