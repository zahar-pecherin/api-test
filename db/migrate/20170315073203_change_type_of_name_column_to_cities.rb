class ChangeTypeOfNameColumnToCities < ActiveRecord::Migration[5.0]
  def change
    change_column :cities, :name, :string
  end
end
