class CreateMembers < ActiveRecord::Migration[5.0]
  def change
    create_table :members do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :id_number
      t.string :student_number
      t.string :profile_image
      t.date :date_of_birth
      t.datetime :joined
      t.datetime :expires
      t.boolean :status
      t.string :signup_source
      t.integer :referrals
      t.string :referral_code
      t.integer :referred_by
      t.string :gender

      t.timestamps null: false
    end
    add_index :members, :referred_by
    add_index :members, :gender
  end
end
