class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.string :reference_number
      t.datetime :date_paid
      t.string :payment_source
      t.decimal :amount_paid

      t.timestamps null: false
    end
  end
end
