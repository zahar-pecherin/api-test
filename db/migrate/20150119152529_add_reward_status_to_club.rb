class AddRewardStatusToClub < ActiveRecord::Migration[5.0]
  def change
    add_column :clubs, :reward_status, :boolean
  end
end
