class AddStudentEmailToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :student_email, :string
  end
end
