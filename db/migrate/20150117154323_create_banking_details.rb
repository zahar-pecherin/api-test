class CreateBankingDetails < ActiveRecord::Migration[5.0]
  def change
    create_table :banking_details do |t|
      t.string :account_name
      t.string :bank_name
      t.string :account_number
      t.string :branch_code
      t.references :member, index: true

      t.timestamps null: false
    end
    add_foreign_key :banking_details, :members
  end
end
