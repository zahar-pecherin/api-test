class AddOtpToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :otp, :integer
  end
end
