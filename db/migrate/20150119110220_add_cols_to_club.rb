class AddColsToClub < ActiveRecord::Migration[5.0]
  def change
    add_column :clubs, :open_date, :date
    add_column :clubs, :close_date, :date
    remove_column :clubs, :logo, :string
    add_reference :clubs, :media, index: true
    add_foreign_key :clubs, :media, column: :media_id
  end
end
