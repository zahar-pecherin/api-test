class AddCityToMember < ActiveRecord::Migration[5.0]
  def change
    add_reference :members, :city, index: true
  end
end
