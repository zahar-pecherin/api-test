class AddCityToUniversities < ActiveRecord::Migration[5.0]
  def change
    add_reference :universities, :city, index: true
  end
end
