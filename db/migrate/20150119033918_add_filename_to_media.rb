class AddFilenameToMedia < ActiveRecord::Migration[5.0]
  def change
    add_column :media, :file_name, :string
  end
end
