class AddSuburbToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :postal_suburb, :string
  end
end
