class CreateLocations < ActiveRecord::Migration[5.0]
  def change
    create_table :locations do |t|
      t.string :name
      t.text :address
      t.string :longitude
      t.string :latitude
      t.references :merchant, index: true

      t.timestamps null: false
    end
    add_foreign_key :locations, :merchants
  end
end
