class AddMemberClubsIdFromPayments < ActiveRecord::Migration[5.0]
  def change
    add_reference :payments, :members_club, index: true
    #add_foreign_key :payments, :members_clubs
  end
end
