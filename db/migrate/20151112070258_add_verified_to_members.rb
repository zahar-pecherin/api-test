class AddVerifiedToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :verified, :boolean, :default => false, index: true
  end
end
