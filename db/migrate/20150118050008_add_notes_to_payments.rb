class AddNotesToPayments < ActiveRecord::Migration[5.0]
  def change
    add_column :payments, :notes, :text

    # basically rename members_clubs to memberships
    remove_reference :payments, :members_club
    #remove_foreign_key :payments, :members_club

    add_reference :payments, :membership, index: true
    add_foreign_key :payments, :memberships

  end
end
