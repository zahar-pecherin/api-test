class RemoveReferralFromMember < ActiveRecord::Migration[5.0]
  def change
    remove_column :members, :referral_code, :string
  end
end
