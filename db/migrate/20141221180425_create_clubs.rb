class CreateClubs < ActiveRecord::Migration[5.0]
  def change
    create_table :clubs do |t|
      t.string :name
      t.boolean :status
      t.string :logo
      t.decimal :price
      t.string :reward
      t.string :type
      t.datetime :start_date
      t.datetime :end_date

      t.timestamps null: false
    end
  end
end
