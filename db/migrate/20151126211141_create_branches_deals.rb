class CreateBranchesDeals < ActiveRecord::Migration[5.0]
  def change
    create_table :branches_deals do |t|
      t.belongs_to :branch, index: true
      t.belongs_to :deal
    end

    # add_index(:branches_deals, :deal_id)
  end

  def down
    drop_table :branches_deals
  end
end
