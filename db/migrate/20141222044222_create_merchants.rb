class CreateMerchants < ActiveRecord::Migration[5.0]
  def change
    create_table :merchants do |t|
      t.string :name
      t.string :merchant_type
      t.text :deal_description
      t.string :logo
      t.string :discount
      t.string :phone
      t.string :email
      t.text :operating_hours

      t.timestamps null: false
    end
  end
end
