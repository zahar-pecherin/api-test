class AddPaymentsToMembersClubs < ActiveRecord::Migration[5.0]
  def change
    add_column :members_clubs, :paid, :boolean
    add_column :members_clubs, :expires, :date
    add_column :members_clubs, :status, :boolean
    add_reference :members_clubs, :payments, index: true
    # add_foreign_key :members_clubs, :payments
  end
end
