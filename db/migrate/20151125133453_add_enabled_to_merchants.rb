class AddEnabledToMerchants < ActiveRecord::Migration[5.0]
  def change
    add_column :merchants, :enabled, :boolean, default: false
  end
end
