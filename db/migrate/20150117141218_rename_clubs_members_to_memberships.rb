class RenameClubsMembersToMemberships < ActiveRecord::Migration[5.0]
  def change
    rename_table :members_clubs, :memberships
  end
end
