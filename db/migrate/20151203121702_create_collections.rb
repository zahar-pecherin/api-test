class CreateCollections < ActiveRecord::Migration[5.0]
  def up
    create_table :collections do |t|
      t.belongs_to :club, index: true
      t.integer :agent_id
      t.integer :member_id
      t.decimal :amount_paid
      t.timestamps null: false
    end
  end

  def down
    drop_table :collections
  end
end
