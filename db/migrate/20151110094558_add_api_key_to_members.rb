class AddApiKeyToMembers < ActiveRecord::Migration[5.0]
  def change
    add_column :members, :api_key, :string, index: true
  end
end
