class RemoveColumnsFromMember < ActiveRecord::Migration[5.0]
  def change
    remove_column :members, :referrals, :integer
    remove_column :members, :referred_by, :integer
    remove_column :members, :signup_source, :string
    remove_column :members, :joined, :text
    remove_column :members, :expires, :text
  end
end
