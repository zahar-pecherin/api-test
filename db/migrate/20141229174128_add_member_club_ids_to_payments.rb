class AddMemberClubIdsToPayments < ActiveRecord::Migration[5.0]
  def change
    add_reference :payments, :members, index: true
    #add_foreign_key :payments, :members
    add_reference :payments, :clubs, index: true
    #add_foreign_key :payments, :clubs
  end
end
