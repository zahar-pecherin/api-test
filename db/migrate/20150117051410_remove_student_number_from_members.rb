class RemoveStudentNumberFromMembers < ActiveRecord::Migration[5.0]
  def change
    remove_column :members, :student_number, :string
  end
end
