class AddDealsMigration < ActiveRecord::Migration[5.0]
  def up
    create_table :deals do |t|
      t.belongs_to :merchant, index: true
      t.string :title
      t.text :blurb
      t.text :deal
      t.text :note
      t.text :terms
      t.boolean :recommended, :default => false
      t.boolean :online, :default => false
      t.boolean :in_store, :default => false
      t.boolean :published, :default => false
      t.string :twitter
      t.string :facebook
      t.string :instagram
      t.string :pinterest
      t.timestamps :ends
      t.timestamps null: false
    end

    add_index :deals, :title
    add_index :deals, :deal
    add_index :deals, :published
    add_index :deals, :in_store
    add_index :deals, :online
    add_index :deals, :recommended
  end

  def down
    drop_table :deals
  end
end
