class RemoveMemberClubIdsFromPayments < ActiveRecord::Migration[5.0]
  def change
    remove_reference :payments, :members, index: true
    #remove_foreign_key :payments, :members
    remove_reference :payments, :clubs, index: true
    #remove_foreign_key :payments, :clubs
  end
end
