class AddMediaToDeals < ActiveRecord::Migration[5.0]
  def change
    add_column :deals, :media, :string
  end
end
