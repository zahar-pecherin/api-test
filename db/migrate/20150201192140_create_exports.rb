class CreateExports < ActiveRecord::Migration[5.0]
  def change
    create_table :exports do |t|
      t.text :contents
      t.string :export_type

      t.timestamps null: false
    end
  end
end
