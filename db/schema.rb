# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170315084047) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "banking_details", force: :cascade do |t|
    t.string   "account_name"
    t.string   "bank_name"
    t.string   "account_number"
    t.string   "branch_code"
    t.integer  "member_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.integer  "account_type"
    t.index ["member_id"], name: "index_banking_details_on_member_id", using: :btree
  end

  create_table "branches", force: :cascade do |t|
    t.integer  "merchant_id"
    t.text     "address"
    t.text     "phone"
    t.text     "email"
    t.decimal  "lng"
    t.decimal  "lat"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.string   "name"
    t.integer  "city_id"
    t.index ["merchant_id"], name: "index_branches_on_merchant_id", using: :btree
  end

  create_table "branches_deals", force: :cascade do |t|
    t.integer "branch_id"
    t.integer "deal_id"
    t.index ["branch_id"], name: "index_branches_deals_on_branch_id", using: :btree
    t.index ["deal_id"], name: "index_branches_deals_on_deal_id", using: :btree
  end

  create_table "cities", force: :cascade do |t|
    t.string   "name"
    t.boolean  "status",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "club_merchants", force: :cascade do |t|
    t.string   "discount"
    t.text     "deal"
    t.integer  "club_id"
    t.integer  "merchant_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["club_id"], name: "index_club_merchants_on_club_id", using: :btree
    t.index ["merchant_id", "club_id"], name: "index_club_merchants_on_merchant_id_and_club_id", unique: true, using: :btree
    t.index ["merchant_id"], name: "index_club_merchants_on_merchant_id", using: :btree
  end

  create_table "clubs", force: :cascade do |t|
    t.string   "name"
    t.boolean  "status"
    t.decimal  "price"
    t.string   "reward"
    t.string   "club_type"
    t.datetime "start_date"
    t.datetime "end_date"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.date     "open_date"
    t.date     "close_date"
    t.integer  "media_id"
    t.boolean  "reward_status"
    t.string   "mailchimp_list"
    t.index ["media_id"], name: "index_clubs_on_media_id", using: :btree
  end

  create_table "collections", force: :cascade do |t|
    t.integer  "club_id"
    t.integer  "agent_id"
    t.integer  "member_id"
    t.decimal  "amount_paid"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["club_id"], name: "index_collections_on_club_id", using: :btree
  end

  create_table "deals", force: :cascade do |t|
    t.integer  "merchant_id"
    t.string   "title"
    t.text     "blurb"
    t.text     "deal"
    t.text     "note"
    t.text     "terms"
    t.boolean  "recommended",     default: false
    t.boolean  "online",          default: false
    t.boolean  "in_store",        default: false
    t.boolean  "published",       default: false
    t.string   "twitter"
    t.string   "facebook"
    t.string   "instagram"
    t.string   "pinterest"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.string   "media"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "url"
    t.boolean  "premium",         default: false
    t.text     "seo_keyword"
    t.text     "seo_description"
    t.integer  "ordering",        default: 0
    t.index ["deal"], name: "index_deals_on_deal", using: :btree
    t.index ["in_store"], name: "index_deals_on_in_store", using: :btree
    t.index ["merchant_id"], name: "index_deals_on_merchant_id", using: :btree
    t.index ["online"], name: "index_deals_on_online", using: :btree
    t.index ["published"], name: "index_deals_on_published", using: :btree
    t.index ["recommended"], name: "index_deals_on_recommended", using: :btree
    t.index ["title"], name: "index_deals_on_title", using: :btree
  end

  create_table "exports", force: :cascade do |t|
    t.text     "contents"
    t.string   "export_type"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.text     "address"
    t.string   "longitude"
    t.string   "latitude"
    t.integer  "merchant_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["merchant_id"], name: "index_locations_on_merchant_id", using: :btree
  end

  create_table "media", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string   "file_name"
  end

  create_table "member_favourites", force: :cascade do |t|
    t.integer  "member_id"
    t.integer  "deal_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["deal_id"], name: "index_member_favourites_on_deal_id", using: :btree
    t.index ["member_id"], name: "index_member_favourites_on_member_id", using: :btree
  end

  create_table "members", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "id_number"
    t.date     "date_of_birth"
    t.boolean  "status"
    t.string   "gender"
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
    t.integer  "media_id"
    t.string   "study_region"
    t.text     "postal_address"
    t.string   "postal_city"
    t.string   "postal_province"
    t.string   "postal_code"
    t.string   "mobile_number"
    t.string   "postal_suburb"
    t.string   "api_key"
    t.boolean  "verified",                     default: false
    t.boolean  "is_agent",                     default: false
    t.string   "avatar"
    t.integer  "university_id"
    t.integer  "city_id"
    t.string   "password"
    t.string   "student_email"
    t.boolean  "photo_changed",                default: false
    t.integer  "otp"
    t.string   "reset_password_token"
    t.datetime "reset_password_token_sent_at"
    t.index ["city_id"], name: "index_members_on_city_id", using: :btree
    t.index ["gender"], name: "index_members_on_gender", using: :btree
    t.index ["media_id"], name: "index_members_on_media_id", using: :btree
    t.index ["university_id"], name: "index_members_on_university_id", using: :btree
  end

  create_table "memberships", force: :cascade do |t|
    t.integer  "club_id"
    t.integer  "member_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.boolean  "paid"
    t.date     "expires"
    t.boolean  "status"
    t.integer  "payments_id"
    t.string   "referral_code"
    t.integer  "referrals"
    t.integer  "referred_by"
    t.string   "signup_source"
    t.boolean  "processed_for_referral", default: false, null: false
    t.index ["club_id"], name: "index_memberships_on_club_id", using: :btree
    t.index ["member_id", "club_id"], name: "index_memberships_on_member_id_and_club_id", unique: true, using: :btree
    t.index ["member_id"], name: "index_memberships_on_member_id", using: :btree
    t.index ["payments_id"], name: "index_memberships_on_payments_id", using: :btree
  end

  create_table "merchants", force: :cascade do |t|
    t.string   "name"
    t.string   "merchant_type"
    t.text     "deal_description"
    t.string   "logo"
    t.string   "discount"
    t.string   "phone"
    t.string   "email"
    t.text     "operating_hours"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.boolean  "enabled",          default: false
    t.string   "url"
    t.text     "terms"
    t.text     "how_to_redeem"
  end

  create_table "payments", force: :cascade do |t|
    t.string   "reference_number"
    t.datetime "date_paid"
    t.string   "payment_source"
    t.decimal  "amount_paid"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.text     "notes"
    t.integer  "membership_id"
    t.index ["membership_id"], name: "index_payments_on_membership_id", using: :btree
  end

  create_table "universities", force: :cascade do |t|
    t.string   "name"
    t.boolean  "status",     default: true
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "city_id"
    t.string   "email_slug"
    t.index ["city_id"], name: "index_universities_on_city_id", using: :btree
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  end

  add_foreign_key "banking_details", "members"
  add_foreign_key "club_merchants", "clubs"
  add_foreign_key "club_merchants", "merchants"
  add_foreign_key "clubs", "media", column: "media_id"
  add_foreign_key "locations", "merchants"
  add_foreign_key "member_favourites", "deals"
  add_foreign_key "member_favourites", "members"
  add_foreign_key "members", "media", column: "media_id"
  add_foreign_key "memberships", "clubs"
  add_foreign_key "memberships", "members"
  add_foreign_key "payments", "memberships"
end
