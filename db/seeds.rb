# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Rails.application.eager_load!

user1 = User.create email: 'admin@varsityvibe.co.za', password: 'v1beATv4rs1ty'
user2 = User.create email: 'team@wixelhq.com', password: 'wixeldev'
club1 = Club.create name: 'Varsity Vibe', price: '200', reward: '40', status: true, club_type: 'Student'

# Add Enabled Cities

City.delete_all

City.create([
                {name: 'Cape Town', status: true},
                {name: 'Johannesburg', status: true},
                {name: 'Pretoria', status: true},
                {name: 'Durban', status: true},
                {name: 'Port Elizabeth', status: true},
                {name: 'Potchefstroom', status: true},
                {name: 'Bloemfontein', status: true}
            ])