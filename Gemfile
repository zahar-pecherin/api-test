source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?('/')
  "https://github.com/#{repo_name}.git"
end

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.0.1'
# Use sqlite3 as the database for Active Record
gem 'pg'
# Use Puma as the app server
gem 'puma', '~> 3.0'
# Use SCSS for stylesheets
gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'
# Use CoffeeScript for .coffee assets and views
gem 'coffee-rails', '~> 4.2'
# See https://github.com/rails/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'
# Turbolinks makes navigating your web application faster. Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 2.5'
# bundle exec rake doc:rails generates the API under doc/api.
gem 'sdoc', '~> 0.4.0', group: :doc

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'

# Use mustache for our templates
gem 'adzap-mustache-rails', '~>0.2.3'

# Use offsite-payments for our Payfast payment processing
gem 'offsite_payments'

# Use devise to manage our authentication requirements
gem 'devise'

# File uploads
gem 'carrierwave'
gem 'mini_magick'

# For API
gem 'swagger-docs'

gem 'font-awesome-rails'

gem 'bootstrap-sass', '~> 3.3.6'

gem 'gentelella-rails'

# gem 'twitter-bootstrap-rails'
gem 'haml-rails'

# Job queues
gem 'redis'
gem 'resque'
gem 'resque-scheduler', '~> 2.5.5'

# Manage mailchimp subscriptions
gem 'gibbon', git: 'git://github.com/amro/gibbon.git'

# Manage csv/xls imports
gem 'active_importer'

# Heroku
gem 'rails_12factor', group: :production
# gem 'rails_serve_static_assets', group: :production

gem 'tinymce-rails'

# Cors
gem 'rack-cors', require: false

# Use Unicorn as the app server
# gem 'unicorn'

# Use for pagination
gem 'kaminari'

# Use for forms
gem 'simple_form'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

gem 'whenever', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'dotenv-rails'

  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'brakeman'
  gem 'rails_best_practices'
  gem 'rubocop'
  gem 'spring'
  gem 'faker'
end

group :development do
  # Access an IRB console on exception pages or by using <%= console %> anywhere in the code.
  gem 'listen', '~> 3.0.5'
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring-watcher-listen', '~> 2.0.0'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
