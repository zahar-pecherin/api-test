Rails.application.eager_load!

Rails.application.routes.draw do

  devise_for :users

  get '/api', to: redirect('/apidocs/index.html')

  # -- Wixel

  # API
  namespace :api do
    namespace :v1 do
      resources :members, only: [:index] do
        collection do
          post 'login',                action: :login
          post 'signup',               action: :signup
          get  'is_verified',          action: :is_verified
          get  'email_exists',         action: :email_exists
          get  'refer_friends',        action: :refer_friends
          get  'force_verification',   action: :force_verification
          get  'is_paid',              action: :is_paid
          post 'upload_photo',         action: :upload_photo
          get  'profile',              action: :profile
          post 'update_profile',       action: :update_profile
        end
      end
      resources :deals, only: [:index, :show] do
        member do
          get 'toggle_favourite_deal', action: 'toggle_favourite_deal'
        end
      end
      resources :cities,       only: [:index]
      resources :universities, only: [:index]
      resources :invites,      only: [:create]
      # Members
      post 'memberships/activate',         to: 'memberships#activate'
      post 'memberships/otp_check',        to: 'memberships#activate_with_otp'
      post 'memberships/ref_code_check',   to: 'memberships#activate_with_ref_code'
    end
  end

  namespace :admin do
    resources :deals do
      put '/change-status/:status', to: 'deals#toggle_status', constraints: { status: /recommended|online|in_store|published/ }
      member do
        get 'add_branch',    action: 'add_branch'
        get 'remove_branch', action: 'remove_branch'
      end
    end
    resources :merchants do
      resources :branches, only: [:new, :edit]
    end
    resources :branches, except: [:index, :show, :new, :edit]
    resources :cities
    resources :universities
    resources :collections, only: [:index]
    resources :payments
    resources :memberships do
      member do
        get 'payment_details', action: :payment_details
      end
      collection do
        get 'set_as_paid', action: :set_as_paid, as: :bulk_paid
        get 'mail/:mail_request', action: :notify, as: :mail
        match 'report', action: :report, as: :report, via: [:get, :post]
        get 'export/:club_id', action: :export, as: :export
      end
    end
    resources :members, except: [:new] do
      member do
        put 'toggle', action: :toggle_agent
        get 'banking_panel', action: :banking_panel
      end
      collection do
        get '/new/(:id_number)', to: 'members#new', as: :new
      end
    end

    get '/exports', to: 'exports#index', as: 'exports'
    get '/exports/:id/download', to: 'exports#download', as: 'export_download'
    get '/exports/referrals', to: 'exports#referrals', as: 'export_referrals'
    get '/imports', to: 'imports#index', as: 'imports'
    post '/imports', to: 'imports#create'
    get 'imports/sample', to: 'imports#sample', as: 'imports_sample'

    # Reporting
    get 'reports/recon', to: 'reports#recon', as: 'recon_report', :defaults => {:format => :csv}
  end

  # -- /Wixel

  # Handle bank account assignment
  # -- Depreciated -- get 'referralcodes/:referral_code', to: 'memberships#refferalcode', as: 'refferalcode'
  get 'banking_details/signup/:referral_code', to: 'banking_details#new_by_referral', as: 'new_signup_banking_details'
  post 'banking_details/signup/:referral_code', to: 'banking_details#create_by_referral'
  patch 'banking_details/signup/:referral_code', to: 'banking_details#update_by_referral'

  resources :banking_details

  # -- Depreciated --  get 'clubs/:id/members', to: 'clubs#members', as: 'membership_list'
  # -- Depreciated --  get 'clubs/:id/merchants', to: 'clubs#merchants', as: 'club_merchant_list'
  get 'payments/missing', to: 'payments#missing_details', as: 'payment_missing_details'

  # -- Depreciated -- get '/memberships/club/:club_id', to: 'memberships#club', as: 'club_memberships'
  # -- Depreciated -- get '/memberships/member/:member_id', to: 'memberships#member', as: 'member_memberships'

  resources :payments do
    post 'complete' => 'payment#complete', as: 'complete'
    get  'success'  => 'payment#success',  as: 'success'
    get  'fail'     => 'payment#fail',     as: 'fail'
  end

  get 'payments/membership/:membership_id', to: 'payments#process_for_membership', as: 'payment_process_for_membership'

  resources :club_merchants
  resources :locations
  resources :merchants
  resources :members
  resources :memberships
  resources :clubs
  resources :media_contents, only: [:create]

  #get '/cards/:referral_code', to: 'cards#index', as: 'cards'
  get '/admin', to: 'admin/dashboards#index', as: 'admin_home'

  #root to: "signup#index", club_id: 1 #signup_club_path(1) #"admin/clubs#index"

  #get 'signup/club/:club_id',            to: 'signup#index',                  as: 'signup_club'
  #post 'signup/club/:club_id',           to: 'signup#create_signup_club',     as: 'signup_create_club'
  #get 'signup/referral/:referral_code',  to: 'signup#referral',               as: 'signup_referral'
  #post 'signup/referral/:referral_code', to: 'signup#create_signup_referral', as: 'signup_create_referral'
  #post 'invite_friend/:referral_code',   to: 'signup#invite_friend',          as: 'signup_invite_friend'
  #get 'signup/success_test',             to: 'signup#success_page_test',      as: 'signup_success_test'

  root to: "website#index", club_id: 1

  #--- Explicit static routing

  get  '/about',                  to: 'website#about'
  get  '/deals',                  to: 'website#index'
  get  '/welcome',                to: 'website#welcome'
  get  '/recruit',                to: 'website#recruit_earn'
  get  '/join',                   to: 'website#join'
  post '/join',                   to: 'website#do_join'
  get  '/become-a-partner',       to: 'website#become_parter'
  get  '/verify/:referral_code',  to: 'website#verify'
  post  '/verify',                to: 'website#verify'
  get  '/verified',               to: 'website#verified'
  get  '/payment/:referral_code', to: 'website#payment'
  get  '/privacy',                to: 'website#privacy'
  get  '/terms',                  to: 'website#terms'
  get  '/test-mails',             to: 'test#mailers'
  get  '/banking/:referral_code', to: 'website#banking'
  post '/banking/:referral_code', to: 'website#save_banking'
  get  '/app',                    to: 'website#getapp'
  get  '/forgot-password',        to: 'website#forgot_password'
  post '/forgot-password',        to: 'website#forgot_password'
  get  '/reset-password',         to: 'website#reset_password'
  post '/reset-password',         to: 'website#reset_password'
  get '/member-info',             to: 'website#member_information'
  post '/member-info',            to: 'website#member_information'

  # Payfast
  #get  '/transaction/start',   to: 'transactions#payfast'
  #get  '/transaction/cancel',  to: 'transactions#cancel'
  #get  '/transaction/fail',    to: 'transactions#fail'
  #get  '/transaction/success', to: 'transactions#success'

  # Payment Flow
  get '/signup/:referral_code/payment', to: 'signup#payment', as: 'signup_payment'

  scope 'pay_fast' do
    post 'paid'                        => 'payments#complete', as: :payfast_paid
    get  'success'                     => 'payments#success',  as: :payfast_success
    get  'fail'                        => 'payments#fail',     as: :payfast_fail
    get  'cancel/:referral_code'       => 'payments#cancel',   as: :payfast_cancel
    get  'process/:club_id/:member_id' => 'payments#process',  as: :payfast_process
  end

  #--- /Explicit static routing

  get '/recover-account', to: redirect('/forgot-password')

end
