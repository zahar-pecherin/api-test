class AutomatedUnpaidEmailJob < ApplicationJob
  queue_as :default

  def perform(membership)
    updated_membership = Membership.find(membership.id)
    MembershipMailer.automated_unpaid_email(updated_membership).deliver_now if updated_membership.id.present? && !updated_membership.paid
  end
end
