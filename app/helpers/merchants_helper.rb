module MerchantsHelper
  def clear_str(str)
    str.delete(',').gsub('&quot', '').split(/[\r\t\n]/).join('')
  end
end
