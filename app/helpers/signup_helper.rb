module SignupHelper
  def signup_form_url
    url_for controller: 'signup', action: 'create_signup_club', club_id: @club.id
  end
end
