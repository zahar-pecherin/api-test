module ApplicationHelper
  def avatar_url(media)
    if media.nil?
      image_path('admin/pixel-admin/avatar.png')
    else
      media.file_name
    end
  end

  def format_date_for_datepicker(dateobject)
    dateobject.strftime('%d/%m/%Y') unless dateobject.nil?
  end

  def format_datetime_for_list(dateobject)
    dateobject.strftime('%d/%m/%Y %I:%M:%S %p') unless dateobject.nil?
  end

  def format_price_for_display(amount)
    number_to_currency(amount, unit: 'R', separator: '.', delimiter: '', precision: 0)
  end

  def temp_card_expiry_date(membership)
    dateobject = membership.payment_date_paid + 14.days
    dateobject.strftime('%d/%m/%Y') unless dateobject.nil?
  end

  def card_print_vars(membership)
    dateobject = membership.payment_date_paid + 14.days

    content_for(:referral_code) { membership.referral_code }
    content_for(:id_number) { membership.member_id_number }
    content_for(:expiry) { dateobject.strftime('%d/%m/%Y') }
    content_for(:member_name) { membership.member_first_name + ' ' + membership.member_last_name }
    content_for(:reward_price) { format_price_for_display(membership.club_reward) }
  end

  def calculate_amount_owing(referrer, counts)
    number_to_currency((referrer.club.reward.to_f * counts[referrer.id]), unit: '', separator: '.', delimeter: '', precision: 2)
  end

  def helper_generate_order_number(refcode)
    num = 1 + Random.rand(999)
    refcode + '-' + num.to_s
  end

  def student_number_from_email(email)
    email.split('@').first
  end

  def per_page_options
    [10, 25, 50, 100]
  end

  def study_region_options
    ['All', 'Cape Town', 'Johannesburg', 'Pretoria', 'Durban', 'Port Elizabeth', 'Potchefstroom', 'Bloemfontein', 'Other']
  end

  def index_search_sort_status_path(params, table_name, column, null_last, index = 'asc')
    url = "#{send("admin_#{table_name}_path")}?order[column]=#{column}&order[index]=#{index}"
    url = "#{url}&order[null_last]=false" if null_last
    url = "#{url}&per_page=#{params[:per_page]}" if params[:per_page]
    url = "#{url}&query[q]=#{params[:query][:q]}" if params[:query]
    url = "#{url}&filter=#{params[:filter]}" if params[:filter]
    url
  end
end
