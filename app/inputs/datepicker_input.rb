class DatepickerInput < SimpleForm::Inputs::Base
  def input(*)
    @builder.text_field(attribute_name, input_html_options.merge(datepicker_options(object.try(attribute_name))))
  end

  def datepicker_options(value = nil)
    { value: value.nil? ? nil : value.to_date.to_formatted_s(:rfc822) }
  end
end
