class BankingDetailsController < ApplicationController
  before_action :set_banking_detail, only: [:show, :edit, :update, :destroy]
  layout 'signup'

  respond_to :html, :json

  def index
    @banking_details = BankingDetail.all
    respond_with(@banking_details)
  end

  def show
    respond_to do |format|
      format.html { render :index }
      format.json { render :show, status: :created }
    end
  end

  def new
    @banking_detail = BankingDetail.new
    respond_with(@banking_detail)
  end

  def edit
    respond_to do |format|
      format.html { render :index }
    end
  end

  def create
    @banking_detail = BankingDetail.new(banking_detail_params)
    @banking_detail.save
    @member = @banking_detail.member
    respond_to do |format|
      format.html { render 'thanks' }
      format.json { redirect_back(fallback_location: root_path) }
    end
  end

  def update
    @banking_detail.update(banking_detail_params)
    @member = @banking_detail.member
    respond_to do |format|
      format.html { render 'thanks' }
      format.json { redirect_back(fallback_location: root_path) }
    end
  end

  # def index
  #   head 404, content_type: "text/html"
  # end
  #
  # def show
  #   head 404, content_type: "text/html"
  # end

  def new_by_referral
    @membership = Membership.includes(:member, :club).find_by_referral_code(params[:referral_code])
    if @membership.nil?
      head 404, content_type: 'text/html'
    else
      @banking_details = BankingDetail.where(member_id: @membership.member.id).first_or_initialize
      @banking_details.member = @membership.member if @banking_details.id.nil?
      respond_to do |format|
        format.html { render 'new' }
      end
    end
  end

  def destroy
    @banking_detail.destroy
    respond_with(@banking_detail)
  end

  private

  def set_banking_detail
    @banking_detail = BankingDetail.find(params[:id])
  end

  def banking_detail_params
    params.require(:banking_detail).permit(:account_name, :bank_name, :account_number, :branch_code, :member_id, :account_type)
  end
end
