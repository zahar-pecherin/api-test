class PaymentsController < ApplicationController
  skip_before_action :verify_authenticity_token, only: [:complete]
  before_action :set_payment, only: [:show, :edit, :update, :destroy]
  before_action :check_for_membership, only: [:create, :process_for_membership]
  layout 'website'

  # GET /payments
  # GET /payments.json
  def index
    @payments = Payment.all
  end

  def process_for_membership
    @payment = Payment.includes(:membership).where(membership_id: params[:membership_id]).first_or_initialize
    @payment.membership = Membership.find(params[:membership_id]) unless @payment.id
    respond_to do |format|
      format.json { render 'show', status: :ok }
    end
  end

  # GET /payments/1
  # GET /payments/1.json
  def show; end

  # GET /payments/new
  def new
    @payment = Payment.new
  end

  # GET /payments/1/edit
  def edit; end

  # POST /payments
  # POST /payments.json
  def create
    init_new_payment

    respond_to do |format|
      if @success
        format.html { redirect_to @payment, notice: 'Payment was successfully created.' }
        format.json { redirect_back(fallback_location: root_path) }
      else
        format.html { render :new }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  def process_payment
    @payment = Payment.new
    @membership = Membership.find_by_referral_code(params[:custom_str1])

    # member = @membership.member
    #
    # if member
    #   member.verified = true
    #   member.save
    # end

    add_new_payment_to_membership
    save_payfast_details
    mark_membership_as_paid
    send_signup_email
    process_referral
  end

  # PATCH/PUT /payments/1
  # PATCH/PUT /payments/1.json
  def update
    respond_to do |format|
      if @payment.update(payment_params)
        @membership = @payment.membership if @payment.id.present?
        mark_membership_as_paid if @membership.id.present?
        format.html { redirect_to @payment, notice: 'Payment was successfully updated.' }
        format.json { redirect_back(fallback_location: root_path) }
      else
        format.html { render :edit }
        format.json { render json: @payment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /payments/1
  # DELETE /payments/1.json
  def destroy
    @payment.destroy
    respond_to do |format|
      format.html { redirect_to payments_url, notice: 'Payment was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def missing_details
    respond_to do |format|
      format.html { redirect_to payments_url, notice: 'Please specify both the member and club you would like to record the payment for' }
      format.json { render json: { error: 'Missing member and/or club information' }, status: :unprocessable_entity }
    end
  end

  def complete
    create_notification

    if @notification.acknowledge
      render nothing: true
      process_payment if @notification.complete?
    else
      logger.debug @notification.inspect
      head :bad_request
    end
  end

  def success; end

  def fail; end

  def cancel
    @membership = Membership.find_by_referral_code(params[:referral_code])
    @club = @membership.club
    respond_to do |format|
      format.html { render 'fail' }
    end
  end

  private

  def set_payment
    @payment = Payment.find(params[:id])
  end

  def set_membership
    @membership = Membership.includes(:club).find(params[:payment][:membership_id])
  end

  def process_referral
    return unless @membership.parent && @membership.paid
    send_referral_mail?(@membership.parent)
    send_referred_mail(@membership, @membership.parent)
  end

  def create_new_payment
    @payment = Payment.new(payment_params)
  end

  def add_new_payment_to_membership
    @payment.membership = @membership
    @payment.amount_paid = @membership.club.price
  end

  def mark_membership_as_paid
    @membership.member.update_attribute(:verified, true)
    @membership.update_attributes(paid: true, status: true, expires: Date.today + 365)
  end

  def save_payfast_details
    @payment.update_attributes(
      reference_number: params[:pf_payment_id],
      notes: 'Paid online via Payfast',
      payment_source: 'PayFast',
      date_paid: DateTime.now
    )
  end

  def init_new_payment
    create_new_payment
    set_membership

    @success = false
    @errors = {}

    if @membership.payment
      @errors = { error: 'A payment has already been recorded for this Membership' }
    elsif add_new_payment_to_membership && @payment.save && mark_membership_as_paid
      send_signup_email
      process_referral
      @success = true
    else
      @errors = @payment.errors
    end
  end

  def payment_params
    params.require(:payment).permit(:reference_number, :date_paid, :payment_source, :amount_paid, :membership_id, :notes)
  end

  def check_for_membership
    if !params[:membership_id] && !params[:payment][:membership_id]
      respond_to do |format|
        format.html redirect_to payment_missing_details_url
        format.json { render json: { error: 'Missing membership information' }, status: :unprocessable_entity }
      end
    end
  end

  def create_notification
    @notification = OffsitePayments.integration(:pay_fast).notification(request.raw_post, secret: Rails.configuration.offsite_payments['payfast']['password'])
  end

  def send_referral_mail?(referral)
    # MembershipMailer.request_banking_details_email(referral).deliver_now if referral and !referral.banking_details? and (referral.children.where(paid: true).count <= 1)
  end

  def send_referred_mail(referral, membership)
    # MembershipMailer.referral_joined_email(membership).deliver_later if membership and membership.banking_details.present? and referral and referral.paid
  end

  def send_signup_email
    # MembershipMailer.signup_email(@membership.member, @membership.club, @membership).deliver_now
  end
end
