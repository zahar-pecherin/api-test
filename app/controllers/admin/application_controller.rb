module Admin
  class ApplicationController < ::ApplicationController
    # Any controller subclassing this instance will use the
    # /app/views/layouts/admin/application.html.erb
    # layout by default.
    before_action :authenticate_user!

    protected

    def query
      params[:query][:q] if params[:query]
    end
  end
end
