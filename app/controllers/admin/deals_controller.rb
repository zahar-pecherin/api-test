class Admin::DealsController < Admin::ApplicationController
  before_action :set_deal, only: [:show, :edit, :update, :destroy, :toggle_status, :add_branch, :remove_branch]
  before_action :set_merchants, only: [:new, :edit, :create, :update]

  # GET /deals
  # GET /deals.json
  def index
    @deals = Deal.instant_search(params[:query]).select_merchants.table_sorting(params[:order]).page(params[:page]).per(params[:per_page])
  end

  # GET /deals/1
  # GET /deals/1.json
  def show
    redirect_to edit_admin_deal_path(@deal)
  end

  # GET /deals/new
  def new
    @deal = Deal.new
  end

  # PATCH/PUT /deals/1/status.json
  def toggle_status
    attr = :"#{params[:status]}"
    if @deal.update_attribute(attr, !@deal.send(attr))
      render json: { status: 200 }
    else
      render json: { status: :unprocessable_entity }
    end
  end

  # GET /deals/1/edit
  def edit; end

  # POST /deals
  # POST /deals.json
  def create
    @deal = Deal.new(deal_params)

    if @deal.save
      redirect_to edit_admin_deal_path(@deal), notice: 'Deal was successfully created'
    else
      render 'new'
    end
  end

  # PATCH/PUT /deals/1
  # PATCH/PUT /deals/1.json
  def update
    if @deal.update(deal_params)
      redirect_to edit_admin_deal_path(@deal), notice: 'Deal was successfully updated.'
    else
      render 'edit'
    end
  end

  # DELETE /deals/1
  # DELETE /deals/1.json
  def destroy
    @deal.destroy
    redirect_to admin_deals_path(@deal), notice: 'Deal was successfully destroyed.'
  end

  def add_branch
    if @deal
      @branch = Branch.find(params[:branch_id])

      if @branch
        @deal.branches << @branch

        render json: { status: 200 }
      else
        render json: { status: 404, errors: ['Branch could not be found'] }
      end
    else
      render json: { status: 404, errors: ['Deal could not be found'] }
    end
  end

  def remove_branch
    if @deal
      @branch = @deal.branches.find(params[:branch_id])

      @deal.branches.delete(@branch)

      render json: { status: 200 }
    else
      render json: { status: 404, errors: ['Deal could not be found'] }
    end
  end

  private

  def set_deal
    @deal = Deal.find(params[:id] || params[:deal_id])
  end

  def set_merchants
    @merchants = Merchant.all
  end

  def deal_params
    params.require(:deal).permit(
      :id,
      :merchant_id,
      :title,
      :blurb,
      :deal,
      :note,
      :terms,
      :recommended,
      :online,
      :in_store,
      :url,
      :published,
      :twitter,
      :facebook,
      :instagram,
      :pinterest,
      :media,
      :start_date,
      :end_date,
      :premium,
      :seo_keyword,
      :seo_description,
      :ordering
    )
  end
end
