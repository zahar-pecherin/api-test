class Admin::ImportsController < Admin::ApplicationController
  require 'membership_importer'

  # GET /imports
  def index
    @clubs = Club.all
    @import = nil
  end

  # POST /imports
  def create
    @clubs = Club.all
    file = params[:file]
    ext = File.extname(file.original_filename)
    ext.slice!(0) # cut off the '.' in .csv

    @importer = MembershipImporter.new(file.path, extension: ext.intern, params: { club_id: params[:club_id], paid: params[:paid], referred_by: params[:referred_by] })

    begin
      @importer.import
    rescue => e
      @errors = e.message
    end

    respond_to do |format|
      format.html { render 'index', notice: 'Import completed successfully.' }
    end
  end

  def sample
    respond_to do |format|
      format.csv
      format.xls
    end
  end
end
