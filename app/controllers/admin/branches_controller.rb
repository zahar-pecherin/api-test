class Admin::BranchesController < Admin::ApplicationController
  before_action :set_branch, only: [:show, :edit, :update, :destroy]
  before_action :set_cities, only: [:edit, :new]
  before_action :set_merchant, only: [:edit, :new]

  # GET /branches/new
  # GET /branches/new.json
  def new
    @branch = @merchant.branches.new
  end

  # GET /branches/1/edit
  def edit; end

  # POST /branches
  # POST /branches.json
  def create
    @branch = Branch.new(branch_params)
    if @branch.save
      redirect_to :back, notice: 'Branch was successfully created.'
    else
      render json: { errors: @branch.errors, status: :unprocessable_entity }
    end
  end

  # PATCH/PUT /branches/1
  # PATCH/PUT /branches/1.json
  def update
    if @branch.update(branch_params)
      redirect_to :back, notice: 'Branch was successfully updated.'
    else
      render json: { errors: @branch.errors, status: :unprocessable_entity }
    end
  end

  # DELETE /branches/1
  # DELETE /branches/1.json
  def destroy
    @branch.destroy
    redirect_to :back, notice: 'Branch was successfully destroyed.'
  end

  private

  def set_branch
    @branch = Branch.find(params[:id])
  end

  def set_cities
    @cities = City.all
  end

  def set_merchant
    @merchant = Merchant.find(params[:merchant_id])
  end

  def branch_params
    params.require(:branch).permit(:id, :name, :merchant_id, :deal_id, :address, :phone, :email, :lng, :lat, :city_id)
  end
end
