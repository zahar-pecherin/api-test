class Admin::UniversitiesController < Admin::ApplicationController
  before_action :set_university, only: [:show, :destroy, :edit, :update]
  before_action :set_cities, only: [:new, :edit, :create, :update]

  # GET /universities
  # GET /universities.json
  def index
    @universities = University.instant_search(params[:query]).table_sorting(params[:order]).page(params[:page]).per(params[:per_page])
  end

  # GET /universities/1
  # GET /universities/1.json
  def show
    render :edit
  end

  # GET /universities/new
  def new
    @university = University.new
  end

  # GET /universities/1/edit
  def edit; end

  # POST /universities
  # POST /universities.json
  def create
    @university = University.new(university_params)

    if @university.save
      redirect_to edit_admin_university_path(@university), notice: 'University was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /universities/1
  # PATCH/PUT /universities/1.json
  def update
    if @university.update(university_params)
      redirect_to edit_admin_university_path(@university), notice: 'University was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /universities/1
  # DELETE /universities/1.json
  def destroy
    @university.destroy
    redirect_to admin_universities_path, notice: 'University was successfully destroyed.'
  end

  private

  def set_university
    @university = University.find(params[:id])
  end

  def set_cities
    @cities = City.all
  end

  def university_params
    params.require(:university).permit(:id, :name, :status, :city_id, :email_slug)
  end
end
