class Admin::MembersController < Admin::ApplicationController
  before_action :set_member, only: [:show, :edit, :update, :destroy, :banking_panel, :membership_panel]
  before_action :set_by_id_number, only: :new
  before_action :set_universities, only: [:new, :edit, :create, :update]
  before_action :set_cities, only: [:new, :edit, :create, :update]
  before_action :hexdigest_password, only: [:create, :update]

  # GET /members
  # GET /members.json
  def index
    # @memberships = Membership.includes(:club).includes(:member).includes(:banking_detail).all
    @members = Member.all
  end

  def toggle_agent
    @member = Member.find_by_id(params[:id])

    @member.update_attribute(:is_agent, !@member.is_agent) if @member

    respond_to do |format|
      format.json { render json: @member }
    end
  end

  # GET /members/1
  # GET /members/1.json
  def show; end

  # GET /members/new
  def new; end

  # GET /members/1/edit
  def edit
    @universities = University.all
  end

  # POST /members
  # POST /members.json
  def create
    @member = Member.new(member_params)

    respond_to do |format|
      if @member.save
        format.html { redirect_to admin_memberships_url, notice: 'Member was successfully created.' }
        format.json { render :show, status: :created, location: @member }
      else
        format.html { render :new }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /members/1
  # PATCH/PUT /members/1.json
  def update
    respond_to do |format|
      if @member.update(member_params)
        check_and_update_mailchimp_list(@member)
        format.html { redirect_to admin_memberships_url, notice: 'Member was successfully updated.' }
        format.json { render :show, status: :ok, location: @member }
      else
        format.html { render :edit }
        format.json { render json: @member.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /members/1
  # DELETE /members/1.json
  def destroy
    @member.destroy

    respond_to do |format|
      format.html { redirect_to '/admin/memberships', notice: 'Member was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def banking_panel
    @banking_detail = @member.banking_detail || @member.build_banking_detail
  end

  private

  def set_member
    @member = Member.find(params[:id])
  end

  def member_params
    list_params = [:first_name, :last_name, :email, :id_number, :media_id, :date_of_birth, :gender, :mobile_number, :postal_address, :postal_suburb, :postal_city, :university_id, :postal_province, :postal_code, :study_region, :avatar, :student_email, :city_id]
    list_params << :password unless params[:member][:password].blank?
    params.require(:member).permit(list_params)
  end

  def hexdigest_password
    params[:password] = Digest::SHA1.hexdigest(params[:password]) if params[:password]
  end

  def set_by_id_number
    @member = Member.new
    @member.build_banking_detail
    return unless params[:id_number]
    member = Member.find_by_id_number params[:id_number]
    return redirect_to edit_admin_member_path(member.id) if member.present?
    @member.populate_sample_from_id_number(params[:id_number])
  end

  def set_universities
    @universities = University.all
  end

  def set_cities
    @cities = City.all
  end

  def check_and_update_mailchimp_list(member)
    student_email_changes = member.previous_changes[:student_email]
    return unless student_email_changes && !member.verified
    referral_code = Mailchimp.get_ref_code_by('Student Email Verification', student_email_changes.first)
    Mailchimp.delete_from_list('Student Email Verification', student_email_changes.first)
    Mailchimp.subscribe_to_list('Student Email Verification', member, referral_code)
  end
end
