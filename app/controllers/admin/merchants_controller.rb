class Admin::MerchantsController < Admin::ApplicationController
  before_action :set_merchant, only: [:show, :edit, :update, :destroy]
  before_action :set_default_branch, only: [:edit]

  # GET /merchants
  # GET /merchants.json
  def index
    @merchants = Merchant.instant_search(params[:query]).table_sorting(params[:order]).page(params[:page]).per(params[:per_page])
    respond_to do |format|
      format.html
      format.csv do
        headers['Content-Disposition'] = 'attachment; filename=\"merchants-info.csv\"'
        headers['Content-Type'] ||= 'text/csv'
      end
    end
  end

  # GET /merchants/1
  # GET /merchants/1.json
  def show
    redirect_to edit_admin_merchant_path(@merchant)
  end

  # GET /merchants/new
  def new
    @merchant = Merchant.new
  end

  # GET /merchants/1/edit
  def edit
    @merchant = Merchant.find(params[:id])
  end

  # POST /merchants
  # POST /merchants.json
  def create
    @merchant = Merchant.new(merchant_params)

    if @merchant.save
      redirect_to edit_admin_merchant_path(@merchant), notice: 'Merchant was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /merchants/1
  # PATCH/PUT /merchants/1.json
  def update
    respond_to do |format|
      if @merchant.update(merchant_params)
        format.html { redirect_to admin_merchant_url(@merchant), notice: 'Merchant was successfully updated.' }
        format.json { render :show, status: :ok, location: @merchant }
      else
        format.html { render :edit }
        format.json { render json: @merchant.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /merchants/1
  # DELETE /merchants/1.json
  def destroy
    @merchant.destroy
    redirect_to admin_merchants_url, notice: 'Merchant was successfully destroyed.'
  end

  private

  def set_merchant
    @merchant = Merchant.find(params[:id])
  end

  def set_default_branch
    @branch = @merchant.branches.new
  end

  def merchant_params
    params.require(:merchant).permit(:name, :merchant_type, :deal_description, :logo, :discount, :phone, :email, :url, :operating_hours, :terms, :how_to_redeem)
  end
end
