class Admin::ExportsController < Admin::ApplicationController
  def index
    BankingDetail.where('account_name IS NULL OR account_name LIKE ?', 'PLEASE COMPLETE ME').destroy_all
    @exports = Export.where(export_type: 'referrals')
    @referrers = Membership.unprocessed_referrers
    @referral_count = 0
    @referrers.count.each do |counter|
      @referral_count += counter[1]
    end
  end

  def referrals
    @referrals = Membership.unprocessed_referrers
    @counts = @referrals.count
    save_to_exports('referrals')
    mark_referrals_as_processed
    respond_to do |format|
      format.csv do
        response.headers['Content-Disposition'] = 'attachment; filename="' + 'referrals-' + @export.id.to_s + '.csv"'
        render 'referrals'
      end
      # format.xls
    end
  end

  def download
    @export = Export.find(params[:id])
    respond_to do |format|
      format.csv do
        response.headers['Content-Disposition'] = 'attachment; filename="' + 'referrals-' + @export.id.to_s + '.csv"'
        render text: @export.contents
      end
    end
  end

  private

  def mark_referrals_as_processed
    @referrals.each(&:process_children!)
  end

  def save_to_exports(export_type)
    @export = Export.new
    @export.contents = render_to_string(export_type + '.csv')
    @export.export_type = export_type
    @export.save
  end
end
