class Admin::MembershipsController < Admin::ApplicationController
  before_action :set_membership, only: [:show, :edit, :update, :destroy, :payment_details]
  before_action :set_memberships, only: [:notify, :set_as_paid]
  before_action :set_mail_request, only: [:notify]

  # GET /memberships
  # GET /memberships.json
  def index
    @ms = Membership.join_table.instant_search(params[:query]).filter(params[:filter]).page(params[:page]).per(params[:per_page])
    @memberships = @ms.select_members.table_sorting(params[:order])
  end

  def export
    @memberships = Membership.includes(:club, member: :banking_detail).where(club_id: params[:club_id]).order('created_at DESC')
    respond_to do |format|
      format.csv do
        response.headers['Content-Disposition'] = 'attachment; filename="' + 'memberships-' + Time.now.strftime('%Y-%m-%d_%H:%M:%S') + '.csv"'
        render 'export'
      end
    end
  end

  def report
    respond_to do |format|
      format.html { render 'report' }
      format.csv do
        build_report
        response.headers['Content-Disposition'] = 'attachment; filename="' + 'memberships-report-' + Time.now.strftime('%Y-%m-%d_%H:%M:%S') + '.csv"'
        render 'export'
      end
    end
  end

  def notify
    process_mail_request

    respond_to do |format|
      format.html { redirect_back(fallback_location: admin_home_path) }
      format.json { render json: { status: 'sent' }, status: :ok }
    end
  end

  def set_as_paid
    process_bulk_payment

    respond_to do |format|
      format.html { redirect_back(fallback_location: admin_home_path) }
      format.json { render json: { status: 'completed' }, status: :ok }
    end
  end

  # GET /memberships/1
  # GET /memberships/1.json
  def show; end

  # GET /memberships/new
  def new
    @membership = Membership.new
    @membership.build_payment
    @membership.member_id = params[:member_id] if params[:member_id]
  end

  # GET /memberships/1/edit
  def edit
    @membership.build_payment unless @membership.payment
  end

  # POST /memberships
  # POST /memberships.json
  def create
    @membership = Membership.new(membership_params)

    respond_to do |format|
      if @membership.save # and send_signup_mail? and send_referral_mail?
        format.html { redirect_to edit_admin_member_path(@membership.member), notice: 'Membership was successfully created.' }
        format.json { render :show, status: :created, location: @membership }
      else
        format.html { render :new }
        format.json { render json: @membership.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /memberships/1
  # PATCH/PUT /memberships/1.json
  def update
    respond_to do |format|
      if @membership.update(membership_params) # and send_signup_mail? and send_referral_mail?
        format.html { redirect_to edit_admin_member_path(@membership.member), notice: 'Membership was successfully updated.' }
        format.json { render :show, status: :ok, location: @membership }
      else
        format.html { render :edit }
        format.json { render json: @membership.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /memberships/1
  # DELETE /memberships/1.json
  def destroy
    @membership.destroy
    respond_to do |format|
      format.html { redirect_to memberships_url, notice: 'Membership was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def payment_details
    @payment = @membership.payment || @membership.build_payment
  end

  private

  def set_membership
    @membership = Membership.includes(:club, :member, :payment).find(params[:id])
    @membership.build_payment unless @membership.payment
  end

  def set_memberships
    @memberships = Membership.includes(:club, :member, :payment).where(id: params[:membership_ids])
  end

  def set_mail_request
    @mail_request = params[:mail_request]
  end

  def process_mail_request
    @memberships.each do |membership|
      case @mail_request
      when 'auto_unpaid'
        send_automated_unpaid_mail membership
      when 'welcome'
        send_signup_mail membership
      when 'payment_reminder'
        send_payment_reminder_mail membership
      when 'banking'
        send_banking_details_mail membership
      when 'referral'
        send_referral_joined_mail membership
      end
    end
  end

  def process_bulk_payment
    @memberships.each do |membership|
      next unless membership.mark_as_paid
      send_signup_mail(membership)
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def membership_params
    params.require(:membership).permit(:club_id, :member_id, :status, :referred_by, :paid, :referral_code, :referred_by_code, payment_attributes: [:id, :date_paid, :payment_source, :notes, :reference_number])
  end

  def send_signup_mail(membership)
    MembershipMailer.signup_email(membership.member, membership.club, membership).deliver_later if membership.paid
  end

  def send_payment_reminder_mail(membership)
    MembershipMailer.signup_email(membership.member, membership.club, membership).deliver_later unless membership.paid
  end

  def send_banking_details_mail(membership)
    MembershipMailer.request_banking_details_email(membership).deliver_later if !membership.banking_details? && membership.referrals
  end

  def send_automated_unpaid_mail(membership)
    # MembershipMailer.automated_unpaid_email(membership).deliver_later if membership.id.present? and !membership.paid
    AutomatedUnpaidEmailJob.perform_later(membership)
  end

  def send_referral_joined_mail(membership)
    MembershipMailer.referral_joined_email(membership).deliver_later if membership && !membership.paid
  end

  def build_report
    @report = Membership.new
    @report.search_conditions = params[:search]
    @memberships = @report.report
  end
end
