require 'date'
require 'csv'

class Admin::ReportsController < Admin::ApplicationController
  def recon
    @members = Member.all.order(id: :desc)

    respond_to do |format|
      format.csv do
        headers['Content-Disposition'] = "attachment; filename=\"members-#{Time.now.to_date}.csv\""
        headers['Content-Type'] ||= 'text/csv'
      end
    end
  end
end
