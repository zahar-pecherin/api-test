class Admin::CitiesController < Admin::ApplicationController
  before_action :set_city, only: [:show, :destroy, :edit, :update]

  # GET /cities
  # GET /cities.json
  def index
    @cities = City.instant_search(params[:query]).table_sorting(params[:order]).page(params[:page]).per(params[:per_page])
  end

  # GET /cities/1
  # GET /cities/1.json
  def show
    render :edit
  end

  # GET /cities/new
  def new
    @city = City.new
  end

  # GET /cities/1/edit
  def edit; end

  # POST /cities
  # POST /cities.json
  def create
    @city = City.new(city_params)

    if @city.save
      redirect_to edit_admin_city_path(@city), notice: 'City was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /cities/1
  # PATCH/PUT /cities/1.json
  def update
    if @city.update(city_params)
      redirect_to edit_admin_city_path(@city), notice: 'City was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /cities/1
  # DELETE /cities/1.json
  def destroy
    @city.destroy
    redirect_to admin_cities_path, notice: 'City was successfully destroyed.'
  end

  private

  def set_city
    @city = City.find(params[:id])
  end

  def city_params
    params.require(:city).permit(:id, :name, :status)
  end
end
