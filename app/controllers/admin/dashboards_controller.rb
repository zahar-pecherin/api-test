class Admin::DashboardsController < Admin::ApplicationController
  # before_action :set_club, only: [:show, :edit, :update, :destroy]

  # GET /clubs
  # GET /clubs.json
  def index
    calculate_signups
    calculate_referrals
    calculate_demographic
    calculate_geographic
    calculate_top_referrers
  end

  private

  def calculate_signups
    @signups = {
      this_week: Membership.joined_this_week.count,
      this_month: Membership.joined_this_month.count,
      today: Membership.joined_today.count
    }
  end

  def calculate_referrals
    @referrals = {
      this_week: Membership.joined_this_week.count(:referred_by),
      this_month: Membership.joined_this_month.count(:referred_by),
      today: Membership.joined_today.count(:referred_by)
    }
  end

  def calculate_demographic
    @demographic = {
      male: Membership.male.count('members.id'),
      female: Membership.female.count('members.id')
    }
  end

  def calculate_geographic
    @geographic = Member.calculate_geographic
  end

  def calculate_top_referrers
    @top_referrals = []
    refs = Membership.limit(6).top_referrers
    refs.each do |ref|
      next if ref[0].nil?
      member = Membership.includes(:member).find_by_id(ref[0])
      next unless member
      member.num_referrals = ref[1]
      @top_referrals.push(member)
    end
  end
end
