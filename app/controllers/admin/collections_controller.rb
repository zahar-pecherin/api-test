class Admin::CollectionsController < Admin::ApplicationController
  # GET /collections
  def index
    @collections = Collection.instant_search(params[:query]).select_relations.table_sorting(params[:order]).page(params[:page]).per(params[:per_page])
  end
end
