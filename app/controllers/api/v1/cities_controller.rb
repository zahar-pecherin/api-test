class Api::V1::CitiesController < Api::V1::BaseController
  ActionController::Parameters.permit_all_parameters = true

  swagger_controller :cities, 'Cities'

  # ----------------------------------------------------------------------------
  # Fetch and display all deals and optionally order them
  # ----------------------------------------------------------------------------
  #
  # GET /api/v1/cities
  #

  swagger_api :index do
    summary 'Returns all City items'
    notes 'This lists all the active cities'
  end

  def index
    @buffer = City.active.as_json(only: [:id, :name])

    render json: { cities: @buffer, status: 200 }
  end
end
