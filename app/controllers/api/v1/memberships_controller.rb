require 'date'

class Api::V1::MembershipsController < Api::V1::BaseController
  before_action :set_agent, only: [:activate, :activate_with_otp, :activate_with_ref_code]

  swagger_controller :memberships, 'Memberships'
  # ----------------------------------------------------------------------------
  # Perform the member sign up request and return its session
  # ----------------------------------------------------------------------------
  #
  # POST /api/v1/memberships/activate
  #
  swagger_api :activate do
    summary 'Send otp'
    notes ''
    param :query, :api_key, :string, false, "agent's api_key"
    param :query, :referral_code, :integer, false, "member's referral code"
    response :success
    response :unauthorized
    response :not_found
  end

  def activate
    @membership = Membership.where(referral_code: params[:referral_code]).where('paid IS null OR paid = false').first
    if @membership
      @member = @membership.member
      @member.update_attribute(:otp, Random.new.rand(1000..9999))
      MemberMailer.otp_email(@member).deliver_now
      render json: { message: 'OTP sent successfully', status: 200 }
    else
      render json: { status: 404, errors: ['Referral code is invalid'] }, status: 404
    end
  end

  # ----------------------------------------------------------------------------
  # Perform account activation using the member OTP
  # ----------------------------------------------------------------------------
  #
  # POST /api/v1/memberships/otp_check
  #
  swagger_api :activate_with_otp do
    summary 'Activate with otp'
    notes ''
    param :query, :api_key, :string, false, "agent's api_key"
    param :query, :otp, :integer, false, "member's otp"
    response :success
    response :unauthorized
    response :not_found
    response :unprocessable_entity
  end

  def activate_with_otp
    @member = Member.find_by_otp(params[:otp]) if params[:otp].present?
    return render json: { status: 401, errors: ['Account with OTP could not be located'] }, status: 401 unless @member && !@member.is_agent
    if @member.membership.activate_by_agent(@agent)
      MemberMailer.otp_complete(@member).deliver_now
      render json: { message: 'Account successfully activated', status: 200 }
    else
      render json: { message: 'Member already activated', status: 422 }, status: 422
    end
  end

  # ----------------------------------------------------------------------------
  # Perform account activation using the referral code
  # ----------------------------------------------------------------------------
  #
  # POST /api/v1/memberships/ref_code_check
  #
  swagger_api :activate_with_ref_code do
    summary 'Activate with referral code'
    notes ''
    param :query, :api_key, :string, false, "agent's api_key"
    param :query, :referral_code, :string, false, "member's referral code"
    response :success
    response :unauthorized
    response :not_found
    response :unprocessable_entity
  end

  def activate_with_ref_code
    @membership = Membership.find_by_referral_code(params[:referral_code])
    return render json: { status: 401, errors: ['Account with referral code could not be located'] }, status: 401 unless @membership
    if @membership.activate_by_agent(@agent)
      render json: { message: 'Account successfully activated', status: 200 }
    else
      render json: { message: 'Member already activated', status: 422 }, status: 422
    end
  end

  private

  def set_agent
    @agent = Member.find_by_api_key(params[:api_key])
    @agent = nil unless @agent.try(:is_agent)
    render json: { status: 404, errors: ['The requester is not a valid agent'] }, status: 404 unless @agent
  end
end
