class Api::V1::UniversitiesController < Api::V1::BaseController
  ActionController::Parameters.permit_all_parameters = true

  swagger_controller :universities, 'Universities'

  # ----------------------------------------------------------------------------
  # Fetch and display all deals and optionally order them
  # ----------------------------------------------------------------------------
  #
  # GET /api/v1/universities
  #

  swagger_api :index do
    summary 'Returns all University items'
    notes 'This lists all the active universities'
  end

  def index
    @buffer = University.active.as_json(only: [:id, :name, :email_slug, :city_id])
    render json: { universities: @buffer, status: 200 }
  end
end
