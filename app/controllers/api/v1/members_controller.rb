require 'digest/sha1'
require 'digest/md5'
require 'securerandom'

class Api::V1::MembersController < Api::V1::BaseController
  before_action :set_member, except: [:index, :email_exists, :login, :signup]
  before_action :set_referral, only: [:signup]
  before_action :rewrite_params, only: [:login, :signup, :update_profile, :force_verification]
  before_action :clear_emails, only: [:email_exists]

  swagger_controller :members, 'Members'

  # ----------------------------------------------------------------------------
  # Perform the member sign up request and return its session
  # ----------------------------------------------------------------------------
  #
  # POST /api/v1/members/index
  #

  swagger_api :index do
    summary 'Returns Member items'
    notes 'This lists all member'
  end

  def index
    if Rails.env.development?
      @members = Member.eager_load(:membership)

      respond_to do |format|
        format.json { render json: @members }
      end
    end
  end

  # ----------------------------------------------------------------------------
  # Check if the user has verified their account
  # ----------------------------------------------------------------------------
  #
  # GET /api/v1/members/is_verified
  #
  swagger_api :is_verified do
    summary 'Checks member verification'
    notes ''
    param :query, :api_key, :string, false, "member's api_key"
    response :success
    response :not_found
  end

  def is_verified
    if @member
      render json: { verified: @member.verified, status: 200 }
    else
      render json: { verified: false, status: 404, errors: ['Member account not found'] }, status: 404
    end
  end

  # ----------------------------------------------------------------------------
  # Check if a personal email has been registered?
  # ----------------------------------------------------------------------------
  #
  # GET /api/v1/members/email_exists/:email
  #
  swagger_api :email_exists do
    summary 'Checks email is exist'
    notes ''
    param :query, :api_key, :string, false, "member's api_key"
    param :query, :email, :string, false, 'email'
    response :success
    response :not_found
  end

  def email_exists
    if Member.check_exist_email(params[:email], params[:api_key])
      render json: { status: 200, message: 'The specified email address exists' }
    else
      render json: { status: 404, message: 'The specified email address does not exist' }, status: 404
    end
  end

  # ----------------------------------------------------------------------------
  # Return calculated values back to the app
  # ----------------------------------------------------------------------------
  #
  # GET /api/v1/members/refer_friends
  #
  swagger_api :refer_friends do
    summary 'Returns info about referrals'
    notes ''
    param :query, :api_key, :string, false, "member's api_key"
    response :success
    response :not_found
  end

  def refer_friends
    if @member
      membership = @member.membership
      render json: { referrals: membership.referrals,
                     paid_referrals: membership.paid_referrals,
                     unpaid_referrals: membership.unpaid_referrals,
                     status: 200 }
    else
      render json: { status: 404, errors: ['Member account not found'] }, status: 404
    end
  end

  # ----------------------------------------------------------------------------
  # Force a member account to send the verification email
  # ----------------------------------------------------------------------------
  #
  # GET /api/v1/members/force_verification
  #
  swagger_api :force_verification do
    summary 'Verification'
    notes ''
    param :query, :api_key, :string, false, "member's api_key"
    response :success
    response :not_found
  end

  def force_verification
    if @member
      @member.update(member_params)
      membership = @member.membership
      Mailchimp.delete_from_list('Student Email Verification', @member.student_email)
      Mailchimp.subscribe_to_list('Student Email Verification', @member, membership.referral_code)
      render json: { status: 200 }
    else
      render json: { verified: false, status: 404, errors: ['Member account not found'] }, status: 404
    end
  end

  # ----------------------------------------------------------------------------
  # Verify that the student has a paid membership
  # ----------------------------------------------------------------------------
  #
  # GET /api/v1/members/is_paid
  #
  swagger_api :is_paid do
    summary "Checks member's paid"
    notes ''
    param :query, :api_key, :string, false, "member's api_key"
    param :query, :club_id, :integer, false, 'club id'
    response :success
    response :not_found
  end

  def is_paid
    club = Club.find_by_id(params[:club_id])
    if club
      render json: { status: 200, paid: @member.paid_membership_to_club?(club) }
    else
      render json: { status: 404, errors: ['The club could not be located'] }, status: 404
    end
  end

  # ----------------------------------------------------------------------------
  # Get the member profile
  # ----------------------------------------------------------------------------
  #
  # GET /api/v1/members/profile
  #
  swagger_api :profile do
    summary 'Returns profile'
    notes ''
    param :query, :api_key, :string, false, "member's api_key"
  end

  def profile
    render json: { status: 200, profile: get_profile(@member) }
  end

  # ----------------------------------------------------------------------------
  # Update the user profile
  # ----------------------------------------------------------------------------
  #
  # POST /api/v1/members/update_profile
  #
  swagger_api :update_profile do
    summary 'Updates a profile'
    notes ''
    param :query, :api_key, :string, false, "member's api_key"
    param :query, :email, :string, false, 'email'
    param :query, :full_name, :string, false, 'full name'
    param :query, :city_id, :integer, false, 'city id'
    param_list :query, :gender, :string, false, 'gender', %w(Male Female)
    param :query, :mobile_number, :string, false, 'mobile number'
    response :success
    response :bad_request
  end

  def update_profile
    invalid = req!([:email, :full_name, :city_id, :gender, :mobile_number])

    return render json: { status: 400, errors: invalid }, status: 400 if invalid.present?

    if @member.update(member_params)
      render json: { status: 200 }
    else
      render json: { status: 400, errors: @member.errors.full_messages }
    end
  end

  # ----------------------------------------------------------------------------
  # Upload a photo to the specified user account
  # ----------------------------------------------------------------------------
  #
  # POST /api/v1/members/upload_photo
  #
  def upload_photo
    render json: { status: 401, errors: ['You may no longer change your profile photo'] } unless @member.can_change_photo?
    if @member.update(avatar: params[:file], photo_changed: true)
      render json: { status: 200, avatar_url: 'https://varsityvibe.co.za' + @member.avatar.url }
    else
      render json: { status: 400, errors: @member.errors.full_messages }
    end
  end

  # ----------------------------------------------------------------------------
  # Perform the member log in request and return it's session
  # ----------------------------------------------------------------------------
  #
  # POST /api/v1/members/login
  #
  swagger_api :login do
    summary 'Logins and returns a profile'
    notes ''
    param :query, :email, :string, false, 'email'
    param :query, :password, :string, false, 'password'
    response :success
    response :bad_request
    response :not_found
  end

  def login
    invalid = req!([:email, :password])
    return render json: { status: 400, errors: invalid }, status: 400 if invalid.present?

    if params[:password].blank?
      render json: { status: 400, errors: ['The password field cannot be blank'] }, status: 400 # @NOTE: This is here because we have students with no passwords currently in the DB
    else
      @member = Member.find_by(email: params[:email], password: params[:password])
      if @member
        render json: { status:  200, member:  get_profile(@member) }
      else
        render json: { status:  404, errors:  ['Your account details are invalid'] }, status: 404
      end
    end
  end

  # ----------------------------------------------------------------------------
  # Perform the member sign up request and return its session
  # ----------------------------------------------------------------------------
  #
  # POST /api/v1/members/signup
  #
  swagger_api :signup do
    summary 'Signup and returns a profile'
    notes ''
    param :query, :api_key, :string, false, "referral's api_key"
    param :query, :email, :string, false, 'email'
    param :query, :password, :string, false, 'password'
    param :query, :student_email, :string, false, 'student email'
    param :query, :full_name, :string, false, 'full name'
    param :query, :university_id, :integer, false, 'university id'
    param_list :query, :verify_now, :string, false, 'verify now', %w(true)
    response :success
    response :bad_request
  end

  def signup
    invalid = req!([:email, :password, :full_name, :university_id, :student_email])
    invalid += check_emails
    return render json: { status: 400, errors: invalid }, status: 400 if invalid.present?
    @member = Member.create_from_mobile(member_params, @referral)
    if @member
      @membership = @member.membership
      subscribe_to_mailchimp_lists
      render json: { status: 200, profile: get_profile(@member) }
    else
      render json: { status: 400, errors: @member.errors.full_messages }, status: 400
    end
  end

  private

  def check_emails
    email_errors = []
    email_errors << 'Account with specified email address exists' if Member.exists?(email: params[:email]) || Member.exists?(email: params[:student_email])
    email_errors << 'Student email address already registered' if Member.exists?(student_email: params[:student_email])
    email_errors
  end

  def get_profile(member)
    university = member.university || University.first

    @buffer = member.with_membership_as_json.merge(city: member.city.as_json(only: [:id, :name]),
                                                   university: university.as_json(only: [:id, :name, :city_id, :email_slug]),
                                                   club: member.membership.club.as_json(only: [:id, :name, :reward, :price, :club_type]))
  end

  def set_referral
    @referral = Membership.find_by_referral_code(params[:referral_code])
  end

  def set_member
    @member = Member.find_by_api_key(params[:api_key])
  end

  def member_params
    params.permit(:email, :password, :first_name, :last_name, :university_id, :student_email, :gender, :mobile_number, :city_id, :date_of_birth)
  end

  def subscribe_to_mailchimp_lists
    Mailchimp.subscribe_to_list('Signed Up but not Paid', @member, @membership.referral_code)
    Mailchimp.subscribe_to_list('Student Email Verification', @member, @membership.referral_code) if params[:verify_now]
    parent_membership = @membership.parent
    parent_member = parent_membership.try(:member)
    Mailchimp.subscribe_to_list('VV Referrers', parent_member, parent_membership.referral_code) if parent_member && parent_member.banking_detail.nil?
  end

  def rewrite_params
    clear_emails
    split_name
    password_coding
    parse_date
  end

  def first_name(s)
    return if s.blank?
    s.split(' ').first.titleize
  end

  def last_name(s)
    return if s.blank? || !s.include?(' ')
    parts = s.split(' ')
    parts = parts.drop(1)
    parts.join(' ').titleize
  end

  def password_coding
    params[:password] = Digest::SHA1.hexdigest(params[:password]) if params[:password].present?
  end

  def split_name
    params[:first_name] = first_name(params[:full_name]) if params[:full_name]
    params[:last_name] = last_name(params[:full_name]) if params[:full_name]
  end

  def clear_emails
    params[:email] = params[:email].try(:downcase).try(:strip) if params[:email]
    params[:student_email] = params[:student_email].try(:downcase).try(:strip) if params[:student_email]
  end

  def parse_date
    params[:date_of_birth] = DateTime.parse(params[:date_of_birth]) if params[:date_of_birth]
  end
end
