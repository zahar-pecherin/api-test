class Api::V1::InvitesController < Api::V1::BaseController
  ActionController::Parameters.permit_all_parameters = true

  before_action :set_member, only: [:create]

  swagger_controller :invites, 'Invites'

  # ----------------------------------------------------------------------------
  # Perform the user invites
  # ----------------------------------------------------------------------------
  #
  # POST /api/v1/invites
  #

  swagger_api :create do
    summary 'Send invite to member'
    notes ''
    param :query, :api_key, :string, false, "member's api_key"
    param :query, :email, :integer, false, 'email of invited'
    param :query, :name, :integer, false, 'name of invited'
    response :success
    response :not_found
  end

  def create
    @membership = @member.try(:membership)

    if @membership
      new_member = Member.new(member_params)
      Mailchimp.subscribe_to_list('VV Invitations', new_member, @membership.referral_code, "#{@member.first_name} #{@member.last_name}")
      render json: { status: 200, invited: params[:email], referral: @membership.referral_code }
    else
      render json: { status: 404, errors: 'No valid membership could be found' }, status: 404
    end
  end

  private

  def set_member
    @member = Member.find_by_api_key(params[:api_key])
  end

  def member_params
    { email: params[:email], first_name: params[:name] }
  end
end
