class Api::V1::BaseController < ApplicationController
  include Swagger::Docs::Methods

  protect_from_forgery with: :null_session

  before_action :destroy_session
  # before_action :set_club

  def destroy_session
    request.session_options[:skip] = true
  end

  def set_club
    @club = params[:club_id] ? Club.find(params[:club_id]) : Club.first
  end

  def req!(p)
    errors = []
    p.each do |key|
      next if params.key?(key) && params[key].present?
      errors << "The field '#{key}' is required, but missing"
    end
    errors
  end
end
