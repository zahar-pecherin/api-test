class Api::V1::DealsController < Api::V1::BaseController
  include ActionView::Helpers::SanitizeHelper

  ActionController::Parameters.permit_all_parameters = true

  before_action :set_member

  swagger_controller :deals, 'Deals'
  # ----------------------------------------------------------------------------
  # Fetch and display all deals and optionally order them
  # ----------------------------------------------------------------------------
  #
  # GET /api/v1/deals?city=1&id=20&query=search&limit=1000
  #

  swagger_api :index do
    summary 'Returns Deal items'
    notes 'This lists all dials'
    param :query, :id, :integer, false, 'last deal id'
    param :query, :limit, :integer, false, 'limit'
    param :query, :city_id, :integer, false, 'city_id'
    param_list :query, :sort, :string, false, 'sort', %w(az age recommended favourites)
    param_list :query, :no_featured, :string, false, 'featured', %w(true)
    param :query, :query, :string, false, 'query'
  end

  def index
    featured = !params.key?(:no_featured) || (params[:sort] == 'recommended')

    @deals = Deal.select_for_api(params[:city_id], params[:query], params[:id], params[:limit])
                 .not_recommended(featured)
                 .sorting(params[:sort]).order(ordering: :desc)

    @buffer = @deals.map { |deal| deal.with_relations_as_json(params.key?(:keep_html)) }

    render json: { status: 200, deals: @buffer }
  end

  # ----------------------------------------------------------------------------
  # Fetch individual deal object
  # ----------------------------------------------------------------------------
  #
  # GET /api/v1/deals/:id
  #

  swagger_api :show do
    summary 'Returns all City items'
    notes 'This lists all the active cities'
    param :path, :id, :integer, :required, 'deal id'
    response :not_found
  end

  def show
    @deal = Deal.find_by_id(params[:id])

    if @deal
      render json: { status: 200 }.merge(@deal.with_relations_as_json(params.key?(:keep_html)))
    else
      render json: { status: 404, errors: ['The deal could not be found'] }, status: 404
    end
  end

  swagger_api :toggle_favourite_deal do
    summary 'Toggle favourite deal '
    notes 'This toggle favourite deal'
    param :query, :api_key, :string, :required, "member's api_key"
    param :path, :id, :integer, :required, 'deal id'
    response :not_found
  end

  def toggle_favourite_deal
    @deal = Deal.find_by_id(params[:id])
    if @deal
      favourite = @deal.toggle_favourite_for(@member)
      render json: { status: 200, deal: @deal.as_json.merge(favorite: favourite.persisted?) }
    else
      render json: { status: 404, errors: ['The deal could not be found'] }, status: 404
    end
  end

  private

  def set_member
    @member = Member.find_by_api_key(params[:api_key]) if params[:api_key]
  end
end
