dashboard =
  init: ->
    do @change_per_page
    do @toggle_status
    do @toggle_agent
    do @branch_toggle
    do @new_member
    do @submit_new_edit_form
    do @bulk_action

  change_per_page: ->
    $('#select_per_page').change (e) ->
      per_page = $(@).children("option:selected").attr('value')
      if location.search.match('per_page')
        search = location.search.split('&')
        for param, index in search
          param = param.split('=')[0] + '=' + per_page if param.match('per_page')
          search[index] = param
        search = search.join('&')
      else
        divider = if location.search != '' then '&' else ''
        search = location.search + divider + 'per_page=' + per_page
      location.search = search

  toggle_status: ->
    $('.switch-deal-status').change (e) ->
      id = $(@).parents('tr').attr('data-id')
      status = $(@).attr('data-status')
      $.ajax
        method: 'PUT'
        url: '/admin/deals/' + id + '/change-status/' + status
        dataType: 'JSON'

  toggle_agent: ->
    $('.switch-agent-status').change (e) ->
      id = $(@).parents('tr').attr('data-id')
      $.ajax
        method: 'PUT'
        url: '/admin/members/' + id + '/toggle'
        dataType: 'JSON'

  branch_toggle: ->
    $('input.branch-toggle').change (e) ->
      deal_id = $(@).parents('table').attr('data-deal-id')
      action = if $(@).is(':checked') then 'add_branch' else 'remove_branch'
      $.get('/admin/deals/' + deal_id + '/' + action, {branch_id: $(@).data('branch')})

  new_member: ->
    $('.add-new-member').click (e) ->
      e.preventDefault();
      form = $(@).parents('form')
      id = form.find('input').val()
      if id != ''
        new_action = form.attr('action') + '/' + form.find('input').val()
        location.href = new_action

  submit_new_edit_form: ->
    $('.submit-form-button').click (e) ->
      e.preventDefault();
      target = $(@).attr('data-target')
      console.log target
      $("#form-#{target}").submit()

  bulk_action:->
    $("#bulk-actions a").click (e) ->
      e.preventDefault()
      check_ids = for item in $('.check-item:checked')
        item.value
      if check_ids.length > 0
        ids = (for id in check_ids
            'membership_ids[]=' + id).join('&')
        location.href = @.href + '?' + ids
          
$(document).on 'turbolinks:load', ->
  do dashboard.init