window.gmap =

  init_map: ->
    currentLat = -33.9253
    currentLng = 18.4239
    # set map center
    if $('#branch_lat').val().length > 0
      currentLat = Number($('#branch_lat').val())
      currentLng = Number($('#branch_lng').val())
    branchLocation = {
      lat: currentLat,
      lng: currentLng
    }
    # Build map
    map = new (google.maps.Map)(
      document.getElementById('map-input'),
      zoom: 9
      center: new google.maps.LatLng(currentLat, currentLng)
      disableDoubleClickZoom: true)
    marker = new (google.maps.Marker)(
      position:
        lat: currentLat
        lng: currentLng
      map: map
      title: 'Branch location')
    if !$('#branch_lat').val().length > 0
      $('#branch_lat').val currentLat
      $('#branch_lng').val currentLng
    # Change location on click
    google.maps.event.addListener map, 'dblclick', (event) ->
      latVar = event.latLng.lat()
      lngVar = event.latLng.lng()
      # populate inputs
      $('#branch_lat').val latVar
      $('#branch_lng').val lngVar
      # change marker pin location
      latlng = new (google.maps.LatLng)(latVar, lngVar)
      marker.setPosition latlng
      return

    idleListener = google.maps.event.addListener(map, 'idle', ->
    #important for full map shown
      map.setCenter branchLocation
      google.maps.event.trigger map, 'resize'
      return
    )
    hoverListener = google.maps.event.addListener(map, 'mousemove', ->
      google.maps.event.trigger map, 'resize'
      return
    )

    # auto place marker
    $(document).on 'keyup', '#branch_address', (event) ->
      google.maps.event.trigger map, 'mousemove'
      #console.log('adding address');
      typewatch (->
        api_request 'https://maps.googleapis.com/maps/api/geocode/json?address=' + $('#branch_address').val(), 'get', {}, (data) ->
          latVar = data.results[0].geometry.location.lat
          lngVar = data.results[0].geometry.location.lng
          # populate inputs
          $('#branch_lat').val latVar
          $('#branch_lng').val lngVar
          center = new (google.maps.LatLng)(latVar, lngVar)
          # using global variable:
          map.panTo center
          # change marker pin location
          latlng = new (google.maps.LatLng)(latVar, lngVar)
          marker.setPosition latlng
          return
        return
      ), 1500
      return
    return