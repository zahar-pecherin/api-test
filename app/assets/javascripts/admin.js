// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require turbolinks
//= require admin/dashboard
//= require tinymce-jquery

//= require js/bootstrap.min.js
//= require js/flot/jquery.flot.js
//= require js/flot/jquery.flot.pie.js
//= require js/flot/jquery.flot.orderBars.js
//= require js/flot/jquery.flot.time.min.js
//= require js/flot/date.js
//= require js/flot/jquery.flot.spline.js
//= require js/flot/jquery.flot.stack.js
//= require js/flot/curvedLines.js
//= require js/flot/jquery.flot.resize.js
//= require js/progressbar/bootstrap-progressbar.min.js
//= require js/icheck/icheck.min.js
//= require js/moment/moment.min.js
//= require js/datepicker/daterangepicker.js
//= require js/chartjs/chart.min.js
//= require js/pace/pace.min.js


//= require admin/gmap

$(document).on("turbolinks:load", function(){
    $("input.datepicker").datepicker({dateFormat: "dd M yy"});

    $('.add-auth-button').on('click', function(e){
        $(e.target).hide();
    });

    // Remember menu state for users
    if(localStorage.menu_state){
        console.log('there is localstorage');
        if(localStorage.menu_state === 'nav-sm'){
            $('body').removeClass('nav-md').addClass(localStorage.menu_state);
        } else {
            $('body').removeClass('nav-sm').addClass(localStorage.menu_state);
        }
    }
    $('body').on('menuToggle', function() {
        console.log('menu toggled', $('body').hasClass('nav-sm'));

        if($('body').hasClass('nav-sm')){
            localStorage.setItem('menu_state', 'nav-sm');
        } else {
            localStorage.setItem('menu_state', 'nav-md');
        }
    });
});
