class ApplicationMailer < ActionMailer::Base
  helper :application
  default from: 'info@varsityvibe.co.za'
  layout 'mailer'
end
