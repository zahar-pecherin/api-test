require 'pp'
class MemberMailer < ActionMailer::Base
  def signup_email(m)
    @member = m
    @subject = '[Varsity Vibe] Welcome to Varsity Vibe!'
    mail(to: @member.email, subject: @subject)
  end

  def verification_email(m)
    @member = m
    @subject = '[Varsity Vibe] Verify your email address'
    mail(to: @member.student_email, subject: @subject)
  end

  def banking_details(m)
    @member = m
    @subject = '[Varsity Vibe] We owe you cash'
    mail(to: @member.email, subject: @subject)
  end

  def refer(name, email, i)
    @name = name
    @inviter = i
    @subject = "[Varsity Vibe] You've been invited!"
    mail(to: email, subject: @subject)
  end

  def otp_email(m)
    @member = m
    @subject = '[Varsity Vibe] Your OTP'
    mail(to: @member.email, subject: @subject)
  end

  def otp_complete(m)
    @member = m
    @subject = '[Varsity Vibe] Welcome!'
    mail(to: @member.email, subject: @subject)
  end

  def reset_password(m)
    @member = m
    @subject = '[Varsity Vibe] Reset password'
    mail(to: @member.email, subject: @subject)
  end
end
