class MembershipMailer < ActionMailer::Base
  def signup_email(member, club, membership)
    @member = member
    @club = club
    @membership = membership
    subject = @membership.active? ? 'Welcome to ' + @club.name : "You're 1 step away"
    mail(to: @member.email, subject: subject)
  end

  def request_banking_details_email(membership)
    @membership = membership
    mail(to: @membership.member_email, subject: 'We owe you cash!')
  end

  def automated_unpaid_email(membership)
    @membership = membership
    @club = membership.club
    @member = membership.member
    mail(to: @membership.member_email, subject: "You're 1 step away")
  end

  def referral_joined_email(membership)
    @membership = membership
    @club = membership.club
    @member = membership.member
    mail(to: @membership.member_email, subject: 'We owe you cash!')
  end

  def invitation_email(friend, membership)
    @membership = membership
    logger.debug(puts(friend))
    logger.debug(puts(membership.inspect))
    mail(to: friend, subject: 'Join the vibe!')
  end

  def verification_email(membership)
    @membership = membership
    mail(to: @membership.member_student_email, subject: 'Verify your email address')
  end

  def otp_email(member)
    @member = member
    mail(to: member.email, subject: '[OTP] Varsity Vibe')
  end

  def otp_complete(member)
    @member = member
    mail(to: member.email, subject: '[Welcome] Varsity Vibe')
  end
end
