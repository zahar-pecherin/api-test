module Mailchimp
  @gb ||= Gibbon::Request.new(api_key: '6b97a572532d2a3c4c30554f4c8fac72-us10', debug: false)

  def subscribe_to_list(list_name, member, rcode = nil, rname = nil)
    merge_vars = { FNAME: member.first_name }
    merge_vars[:LNAME] = member.last_name if member.last_name
    merge_vars[:RCODE] = rcode if rcode
    merge_vars[:RNAME] = rname if rname

    begin
      @gb.lists(get_list_id(list_name)).members.create(body: { email_address: choose_email(list_name, member), status: 'subscribed', merge_fields: merge_vars })
    rescue
      nil
    end
  end

  def delete_from_list(list_name, email)
    hashed_email = Digest::MD5.hexdigest(email.downcase)
    begin
      @gb.lists(get_list_id(list_name)).members(hashed_email).delete
    rescue
      nil
    end
  end

  def get_ref_code_by(list_name, email)
    hashed_email = Digest::MD5.hexdigest(email.downcase)
    begin
      previous_record = @gb.lists(get_list_id(list_name)).members(hashed_email).retrieve
      ref_code = previous_record['merge_fields']['RCODE']
    rescue
      ref_code = nil
    end
    ref_code
  end

  def get_list_id(list_name)
    @gb.lists.retrieve['lists'].each { |list| return list['id'] if list['name'] == list_name }
  end

  def choose_email(list_name, member)
    if list_name == 'Student Email Verification'
      member.student_email
    else
      member.email
    end
  end

  module_function :subscribe_to_list, :delete_from_list, :get_list_id, :choose_email, :get_ref_code_by
end
