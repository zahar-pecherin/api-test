# encoding: utf-8
class MerchantUploader < CarrierWave::Uploader::Base
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  storage :file
  # storage :fog

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    'http://placehold.it/450x450'
  end

  version :thumb do
    # process resize_to_fit: [230, 230]
    process resize_to_fill: [230, 230, 'Center']
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end
end
