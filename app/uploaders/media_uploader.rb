# encoding: utf-8
class MediaUploader < CarrierWave::Uploader::Base
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  storage :file
  # storage :fog

  def extension_white_list
    %w(jpg jpeg gif png)
  end
end
