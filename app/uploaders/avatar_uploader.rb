# encoding: utf-8
class AvatarUploader < CarrierWave::Uploader::Base
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  storage :file
  # storage :fog

  before :store, :convert_to_grayscale

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    if model.set_grey_avatar?
      '/img/upload-photo-grey.svg'
    else
      '/img/upload-photo.svg'
    end
  end

  version :thumb do
    process resize_to_fill: [320, 320, 'Center']
  end

  version :copy, if: :check_grey_avatar do
    process resize_to_fill: [720, 720, 'Center']
  end

  process resize_to_fill: [720, 720, 'Center']

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  def convert_to_grayscale(_p)
    return unless model.set_grey_avatar? && !path.include?('/copy_') && !path.include?('/thumb_')
    manipulate! do |img|
      img.colorspace('Gray')
      img.brightness_contrast('-30x0')
      img = yield(img) if block_given?
      img
    end
  end

  def check_grey_avatar(_file)
    model.set_grey_avatar?
  end
end
