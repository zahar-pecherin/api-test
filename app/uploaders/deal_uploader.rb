# encoding: utf-8
class DealUploader < CarrierWave::Uploader::Base
  # include CarrierWave::RMagick
  # include CarrierWave::MiniMagick

  include CarrierWave::MiniMagick

  storage :file
  # storage :fog

  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  def default_url
    nil
  end

  version :thumb do
    # process :resize_to_fit => [480, 330]
    # process resize_to_fit: [480, 330]
    # process crop: '480x330+0+0'
    process resize_to_fill: [480, 330, 'Center']
  end

  version :header do
    # process resize_to_fit: [1000, 330]
    # process crop: '1000x330+0+0'
    process resize_to_fill: [1000, 330, 'Center']
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  private

  def crop(geometry)
    manipulate! do |img|
      img.crop(geometry)
      img
    end
  end

  def resize_and_crop(size)
    manipulate! do |image|
      if image[:width] < image[:height]
        remove = ((image[:height] - image[:width]) / 2).round
        image.shave("0x#{remove}")
      elsif image[:width] > image[:height]
        remove = ((image[:width] - image[:height]) / 2).round
        image.shave("#{remove}x0")
      end
      image.resize("#{size}x#{size}")
      image
    end
  end
end
