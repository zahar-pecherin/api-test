class Merchant < ApplicationRecord
  include InstantSearch
  include TableSorting

  use_field_for_search %w(name merchant_type phone email)

  has_many :locations, inverse_of: :merchant
  has_many :branches, dependent: :destroy
  has_many :club_merchants
  has_many :clubs, through: :club_merchants
  has_many :deals

  mount_uploader :logo, MerchantUploader
end
