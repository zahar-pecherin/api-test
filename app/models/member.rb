require 'digest/sha1'
require 'digest/md5'
require 'securerandom'
# require 'carrierwave/orm/activerecord'

class Member < ApplicationRecord
  has_one :membership, dependent: :destroy
  has_many :clubs, through: :memberships
  has_one :banking_detail, dependent: :destroy
  belongs_to :university
  belongs_to :city
  belongs_to :media, required: false
  has_many :member_favourites, dependent: :destroy
  has_many :favourite_deals, through: :member_favourites, source: :member
  has_many :collections, foreign_key: :agent_id

  validates :first_name, :email, :password, presence: true
  # validates :email, :id_number, uniqueness: {case_sensitive: false}

  mount_uploader :avatar, AvatarUploader

  before_create do
    self.api_key = Digest::SHA1.hexdigest("#{self.email}#{Time.now.to_i}")
  end

  before_save do
    self.email = self.email.downcase unless self.email.nil?
    self.student_email = self.student_email.downcase unless self.student_email.nil?
  end

  after_create :create_membership

  after_create do
    Mailchimp.subscribe_to_list('ALL VV Members', self)
  end

  scope :calculate_geographic, -> { select('members.study_region, COUNT(*) AS members_count').group(:study_region).order('members_count DESC') }

  scope :is_active, -> { where(status: true) }

  after_initialize :defaults

  def self.create_from_mobile(params, referral)
    member = Member.new(params)
    member.city_id = member.university.try(:city).try(:id) || City.first.id
    if member.save
      membership = member.membership

      membership.signup_source = 'Mobile'
      membership.parent = referral if referral.present?
      membership.save
    end
    member
  end

  def self.check_exist_email(email, api_key = nil)
    members = Member.where('email = ? OR student_email = ?', email, email)
    members = members.where.not(api_key: api_key) if api_key
    members.present?
  end

  def can_change_photo?
    self.membership.paid && !self.photo_changed
  end

  def defaults
    # self.joined ||= Date.today
    # self.expires ||= Date.today + 365
    # self.status = false if self.status.nil?
  end

  def belongs_to_active_club?
    clubs.each do |club|
      return true if club.status
    end
    false
  end

  def belongs_to_club(id)
    clubs.each do |club|
      return true if club.id == id
    end
    false
  end

  def paid_membership_to_club?(club)
    membership.club_id == club.id && membership.paid
  end

  def city_id=(val)
    city = City.find_by_id(val)
    write_attribute(:study_region, city.name) if city
    write_attribute(:city_id, val.to_i)
  end

  def populate_sample_from_id_number(id_number)
    self.id_number = id_number
    self.first_name = 'Sample'
    self.last_name = 'Sample'
    self.email = 'Sample'
  end

  def student_number
    email = self.student_email
    email ? email.split('@').first : ''
  end

  def membership_paid?
    self.membership.status
  end

  def membership_never_paid?
    !self.membership.try(:paid)
  end

  def set_grey_avatar?
    !(self.membership_never_paid? || self.membership_paid?)
  end

  # def student_id
  #  student_number
  # end

  def name
    "#{self.first_name} #{self.last_name}"
  end

  def create_reset_password
    time = Time.current
    token = Digest::MD5.hexdigest(self.email + time.to_s)
    update_attribute(:reset_password_token, token)
    update_attribute(:reset_password_token_sent_at, time)
  end

  def active_link?
    (self.reset_password_token_sent_at + 1.day) > Time.current
  end

  def with_membership_as_json
    member_param = [:api_key, :email, :mobile_number, :date_of_birth, :gender, :status, :verified, :is_agent, :photo_changed]
    self.as_json(only: member_param).merge(student_id: self.student_number,
                                           full_name: self.name,
                                           avatar: 'https://varsityvibe.co.za' + self.avatar.url,
                                           valid_membership:  self.paid_membership_to_club?(self.membership.club),
                                           referral_code: self.membership.referral_code,
                                           expiry_date: self.membership.expires)
  end
end
