class City < ApplicationRecord
  include InstantSearch
  include TableSorting

  use_field_for_search ['name']

  scope :active, -> { where(status: true).order(name: :asc) }
end
