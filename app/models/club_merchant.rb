class ClubMerchant < ApplicationRecord
  belongs_to :club
  belongs_to :merchant
end
