class Club < ApplicationRecord
  include InstantSearch

  has_many :memberships
  has_many :members, through: :memberships
  has_many :club_merchants
  has_many :merchants, through: :club_merchants
  belongs_to :media, required: false

  validates :name, presence: true
  scope :currently_open_for_members, -> { where('start_date <= ? AND end_date >= ?', Date.today, Date.today) }
  scope :with_status, lambda { |status| where(status: status == 'active') if status }

  after_initialize :defaults

  def defaults
    self.start_date ||= Date.today
    self.end_date ||= Date.today + 14
    self.status = true if self.status.nil?
  end
end
