class Membership < ApplicationRecord
  include InstantSearch
  include TableSorting

  use_field_for_search %w(members.first_name members.email memberships.referral_code members.student_email parents.referral_code)

  belongs_to :club, required: false
  belongs_to :member
  has_one :payment, inverse_of: :membership, dependent: :destroy

  belongs_to :parent, class_name: 'Membership', foreign_key: 'referred_by', counter_cache: :referrals, required: false
  has_many :children, class_name: 'Membership', foreign_key: 'referred_by'

  accepts_nested_attributes_for :payment, allow_destroy: true

  scope :paid, -> { where(paid: true) }
  scope :unpaid, -> { where(paid: false) }
  scope :active, -> { where(status: true) }
  scope :inactive, -> { where(status: false) }
  scope :individual, ->(club_id, member_id) { where(club_id: club_id, member_id: member_id).first }
  scope :unprocessed_referrals, -> { where('referred_by > ?', 0).where(paid: true).where(processed_for_referral: false) }
  scope :unprocessed_referrers, -> { joins(:children).joins(:member).where(children_memberships: { paid: true, processed_for_referral: false }).group('memberships.id') }
  scope :joined_this_week, -> { where('created_at >= ?', Time.zone.now.beginning_of_week) }
  scope :joined_this_month, -> { where('created_at >= ?', Time.zone.now.beginning_of_month) }
  scope :joined_today, -> { where('created_at >= ?', Time.zone.now.beginning_of_day) }
  scope :joined_this_year, -> { where('created_at >= ?', Time.zone.now.beginning_of_year) }
  scope :male, -> { includes(:member).where('members.gender =?', 'Male') }
  scope :female, -> { includes(:member).where('members.gender =?', 'Female') }
  scope :top_referrers, -> { group(:referred_by).order('count_referred_by DESC').count(:referred_by) }
  scope :filter, ->(filter) { send(filter) if %w(paid unpaid active inactive).include? filter }

  attr_accessor :num_referrals, :search_conditions

  delegate :date_paid, to: :payment, prefix: true, allow_nil: true

  delegate :first_name, to: :member, prefix: true, allow_nil: true
  delegate :last_name, to: :member, prefix: true, allow_nil: true
  delegate :email, to: :member, prefix: true, allow_nil: true
  delegate :student_email, to: :member, prefix: true, allow_nil: true
  delegate :avatar, to: :member, prefix: true, allow_nil: true
  delegate :name, to: :member, prefix: true, allow_nil: true
  delegate :student_number, to: :member, prefix: true, allow_nil: true
  delegate :id_number, to: :member, prefix: true, allow_nil: true
  delegate :verified, to: :member, prefix: true, allow_nil: true
  delegate :created_at, to: :member, prefix: true, allow_nil: true

  delegate :name, to: :club, prefix: true, allow_nil: true
  delegate :reward, to: :club, prefix: true, allow_nil: true

  after_initialize :defaults
  before_create :generate_referral_code
  before_create :set_club

  scope :select_members, lambda {
    paid_members = 'SUM(CASE WHEN childrens.paid IS TRUE THEN 1 ELSE 0 END) AS paid_members'
    unpaid_members = 'SUM(CASE WHEN childrens.paid IS FALSE THEN 1 ELSE 0 END) AS unpaid_members'
    select("members.*, parents.referral_code AS parent_referral_code, banking_details.*, #{unpaid_members}, #{paid_members}, memberships.*")
  }

  scope :join_table, lambda {
    joins('LEFT OUTER JOIN members ON memberships.member_id = members.id')
        .joins('LEFT OUTER JOIN memberships parents ON memberships.referred_by = parents.id')
        .joins('LEFT OUTER JOIN banking_details ON banking_details.member_id = members.id')
        .joins('LEFT OUTER JOIN memberships childrens ON memberships.id = childrens.referred_by ')
        .group('members.id, memberships.id, parents.referral_code, banking_details.id')
  }


  def activate_by_agent(agent)
    return if self.active?
    payment = self.create_or_update_payment(amount_paid: self.club.price,
                                            date_paid: DateTime.now,
                                            reference_number: "Agent (#{agent.email})",
                                            payment_source: 'Vibe Agent',
                                            notes: "Collected via #{agent.email}")
    agent.collections.create(member_id: self.member_id, amount_paid: self.club.price, club_id: self.club_id)
    self.member.update!(verified: true, otp: nil)
    self.update_attributes(payments_id: payment.id, status: true, paid: true, expires: Date.today + 365)
  end

  def create_or_update_payment(params)
    self.payment ? self.payment.update(params) : self.create_payment(params)
    self.payment
  end

  def mark_as_paid
    return if self.paid
    self.member.update_attribute(:verified, true)
    self.create_payment(reference_number: 'BULK UPDATE', payment_source: 'Import')
    self.update_attributes(paid: true, status: true)
  end

  def defaults
    self.paid = false if paid.nil?
    self.status = false if status.nil?
    # self.expires ||= self.club.end_date
  end

  def active?
    self.status && self.paid
  end

  def banking_details?
    self.member.banking_detail.try(:account_name)
  end

  def referred_by_code
    return if self.referred_by.blank?
    referrer = Membership.where(id: self.referred_by).first
    referrer.referral_code if referrer
  end

  def referred_by_code=(code)
    referrer = Membership.where(referral_code: code).first
    self.referred_by = referrer.id if referrer
  end

  # Generates a random string from a set of easily readable characters
  def generate_referral_code(size = 6)
    charset = %w(2 3 4 6 7 9 A C D E F G H J K M N P Q R T V W X Y Z)
    self.referral_code = (0...size).map { charset.to_a[rand(charset.size)] }.join
  end

  def process_children!
    @children = Membership.where(referred_by: self.id, paid: true, processed_for_referral: false).update_all(processed_for_referral: true)
  end

  def paid_referrals
    Membership.where(referred_by: self.id, paid: true).count
  end

  def unpaid_referrals
    Membership.where(referred_by: self.id, paid: false).count
  end

  def total_referrals
    Membership.where(referred_by: self.id).count
  end

  def report
    @report ||= build_report
  end

  private

  def build_report
    Membership.joins(:member).includes(:club, member: :banking_detail).where(conditions)
  end

  def set_club
    self.club_id = Club.first.id unless self.club_id
  end

  # def keyword_conditions
  #   ['members.first_name LIKE ?', "%#{search_conditions[:keywords]}%"] unless search_conditions[:keywords].blank?
  #   ['members.last_name LIKE ?', "%#{search_conditions[:keywords]}%"] unless search_conditions[:keywords].blank?
  # end

  def start_date_conditions
    ['memberships.created_at >= ?', Time.parse(search_conditions[:start_date])] unless search_conditions[:start_date].blank?
  end

  def end_date_conditions
    ['memberships.created_at <= ?', Time.parse(search_conditions[:end_date])] unless search_conditions[:end_date].blank?
  end

  def geographic_conditions
    ['members.study_region = ?', search_conditions[:location]] unless search_conditions[:location].blank? || search_conditions[:location] == 'All'
  end

  def gender_conditions
    ['members.gender = ?', search_conditions[:gender]] unless search_conditions[:gender].blank? || search_conditions[:gender] == 'All'
  end

  def paid_conditions
    ['memberships.paid = ?', true] if search_conditions[:status] == 'Paid'
  end

  def unpaid_conditions
    ['memberships.paid = ?', false] if search_conditions[:status] == 'Unpaid'
  end

  def conditions
    [conditions_clauses.join(' AND '), *conditions_options]
  end

  def conditions_clauses
    conditions_parts.map(&:first)
  end

  def conditions_options
    conditions_parts.map { |condition| condition[1..-1] }.flatten
  end

  def conditions_parts
    private_methods(false).grep(/_conditions$/).map { |m| send(m) }.compact
  end
end
