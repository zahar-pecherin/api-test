class University < ApplicationRecord
  include InstantSearch
  include TableSorting

  use_field_for_search ['name']

  belongs_to :city

  scope :active, -> { where(status: true).order(name: :asc) }
end
