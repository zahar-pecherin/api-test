class Branch < ApplicationRecord
  belongs_to :merchant
  belongs_to :city
  has_and_belongs_to_many :deals

  delegate :name, to: :city, prefix: true, allow_nil: true

  def city_id=(val)
    write_attribute(:city_id, val.to_i)
  end

  def as_json_with_except(exception_list = [])
    self.as_json(except: exception_list).merge(city_name: self.city_name)
  end
end
