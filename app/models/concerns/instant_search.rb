module InstantSearch
  extend ActiveSupport::Concern

  included do
    scope :instant_search, -> (query) do
      if query.present?
        sql = @search_fields.map { |f| "UPPER(#{f}) LIKE UPPER(:query)" }.join(' OR ')
        where(sql, query: "%#{query[:q]}%")
      end
    end
  end

  module ClassMethods
    attr_reader :search_fields

    def search_fields
      @search_fields || []
    end

    private

    def use_field_for_search(fields)
      @search_fields = fields
    end
  end
end
