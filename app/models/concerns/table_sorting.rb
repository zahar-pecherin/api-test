module TableSorting
  extend ActiveSupport::Concern

  included do
    scope :sorted_by_column, lambda { |order|
      null_last = 'NULLS LAST' unless order[:null_last]
      order("#{order[:column]} #{order[:index].upcase} #{null_last}")
    }

    scope :table_sorting, lambda { |order|
      if order
        sorted_by_column(order)
      else
        order(created_at: :desc)
      end
    }
  end
end
