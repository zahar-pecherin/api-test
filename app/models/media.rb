class Media < ApplicationRecord
  has_many :members
  has_many :clubs
  has_many :deals
  mount_uploader :file_name, MediaUploader
end
