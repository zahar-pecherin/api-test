class Deal < ApplicationRecord
  include InstantSearch
  include TableSorting
  include ActionView::Helpers::SanitizeHelper

  use_field_for_search %w(deals.title merchants.name)

  belongs_to :merchant
  has_many :member_favourites, dependent: :destroy
  has_many :members, through: :member_favourites
  has_many :branches

  has_and_belongs_to_many :branches

  scope :search, lambda { |q| where('LOWER(title) LIKE LOWER(:query) OR LOWER(deal) like LOWER(:query) OR LOWER(seo_keyword) like LOWER(:query)', query: "%#{q}%") if q }

  scope :in_city, lambda { |city_id| joins(:branches).where('branches.city_id != ?', city_id).distinct if city_id.present? }

  scope :id_less_than, lambda { |last_id| where('deals.id <= ?', last_id) if last_id }

  scope :select_merchants, lambda {
    select('deals.*, merchants.name AS name_of_merchant')
      .joins('LEFT OUTER JOIN merchants ON deals.merchant_id = merchants.id')
  }

  scope :select_for_api, lambda { |city_id, query, last_id, limit|
    where(published: true)
      .where('DATE(?) BETWEEN start_date AND end_date', DateTime.now)
      .in_city(city_id)
      .search(query)
      .id_less_than(last_id)
      .limit(limit)
  }

  scope :not_recommended, lambda { |featured|
    where(recommended: false) unless featured
  }

  scope :sorting, lambda { |sort|
    case sort
    when 'az'
      joins(:merchant).order('merchants.name ASC')
    when 'age'
      order(start_date: :desc)
    when 'recommended'
      order(recommended: :desc)
    when 'favourites'
      select('deals.*')
        .select('count(member_favourites.*) as favourites')
        .joins('LEFT JOIN member_favourites ON deals.id = member_favourites.deal_id')
        .group('deals.id')
        .order('favourites DESC')
    end
  }

  validates :title, presence: true

  mount_uploader :media, DealUploader

  def start_date=(val)
    if !val.empty?
      write_attribute(:start_date, Date.strptime(val, '%m/%d/%Y'))
    else
      write_attribute(:start_date, nil)
    end
  end

  def end_date=(val)
    if !val.empty?
      write_attribute(:end_date, Date.strptime(val, '%m/%d/%Y'))
    else
      write_attribute(:end_date, nil)
    end
  end

  def merchant_id=(val)
    write_attribute(:merchant_id, val.to_i)
  end

  def toggle_favourite_for(member)
    favorite = self.member_favourites.find_by_member_id(member.id)
    if favorite
      favorite.destroy
    else
      favorite = self.member_favourites.create(member_id: member.id)
    end
    favorite
  end

  def favourite_for(member)
    self.member_favourites.where(member_id: member.id).present?
  end

  def with_relations_as_json(keep_html = false)
    self.build_as_json(keep_html).merge(branches: self.branches.map { |b| b.as_json_with_except([:id, :merchant_id, :created_at, :updated_at]) },
                                        merchant: merchant_as_json(self.merchant))
  end

  def build_as_json(keep_html)
    except_param = [:merchant_id, :note, :updated_at, :media, :url, :ordering]
    self.as_json(except: except_param).merge(blurb: remove_tags(self.blurb, keep_html),
                                             deal: remove_tags(self.deal, keep_html),
                                             terms: remove_tags(self.terms, keep_html),
                                             img_full: self.media ? 'https://varsityvibe.co.za' + self.media.url : nil,
                                             img_thumb: self.media ? 'https://varsityvibe.co.za' + self.media.url(:thumb) : nil,
                                             created_date: self.created_at.strftime('%b %d, %Y'),
                                             created_ts: self.created_at.to_time.to_i,
                                             how_to_redeem: self.merchant ? self.merchant.how_to_redeem : nil,
                                             favorite: @member ? d.favourite_for(@member) : nil)
  end

  def merchant_as_json(merchant)
    {
      name: merchant.try(:name) || '[Unknown]',
      logo: merchant ? 'https://varsityvibe.co.za' + merchant.logo.url : nil,
      phone: merchant.try(:phone),
      email: merchant.try(:email),
      url: merchant.try(:url),
      how_to_redeem:  merchant.try(:how_to_redeem)
    }
  end

  def remove_tags(input, keep_html)
    keep_html ? input : strip_tags(input)
  end
end
