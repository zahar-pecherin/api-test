class Location < ApplicationRecord
  belongs_to :merchant, inverse_of: :locations
  validates :name, :address, :longitude, :latitude, :merchant, presence: true
end
