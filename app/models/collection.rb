class Collection < ApplicationRecord
  include InstantSearch
  include TableSorting

  use_field_for_search %w(agents.first_name members.first_name memberships.referral_code)

  belongs_to :member, dependent: :destroy
  belongs_to :club, dependent: :destroy
  belongs_to :agent, class_name: 'Member', foreign_key: 'agent_id'

  scope :select_relations, lambda {
    select('collections.*, members.first_name AS name_of_member, agents.first_name AS name_of_agent, memberships.referral_code AS referral_code')
      .joins('LEFT OUTER JOIN members agents ON collections.agent_id = agents.id')
      .joins('LEFT OUTER JOIN members ON collections.member_id = members.id')
      .joins('LEFT OUTER JOIN memberships ON members.id = memberships.member_id')
  }
end
