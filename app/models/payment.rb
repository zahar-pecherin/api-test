class Payment < ApplicationRecord
  belongs_to :membership, inverse_of: :payment
  validates :payment_source, presence: true
  after_create :subscribe_to_mailchimp_list, :add_to_wellcome_list, :change_avatar # only subscribe member after payment has been recorded

  after_initialize :defaults

  def defaults
    self.reference_number ||= '' # ||= generate_referral_code(10)
    self.payment_source ||= 'Cash'
  end

  # Generates a random string from a set of easily readable characters
  def generate_referral_code(size = 6)
    charset = %w(2 3 4 6 7 9 A C D E F G H J K M N P Q R T V W X Y Z)
    (0...size).map { charset.to_a[rand(charset.size)] }.join
  end

  # Auto subscribes member to membership/club mailchimp list
  def subscribe_to_mailchimp_list
    return if self.membership.club.mailchimp_list.blank?
    gb = Gibbon::API.new
    options = {
      id: self.membership.club.mailchimp_list,
      email: { email: self.membership.member.email },
      merge_vars: { FNAME: self.membership.member.first_name, LNAME: self.membership.member.last_name },
      double_optin: false
    }
    gb.lists.subscribe(options)
  end

  def add_to_wellcome_list
    membership = self.membership
    member = membership.member
    Mailchimp.delete_from_list('Signed Up but not Paid', member.email)
    Mailchimp.subscribe_to_list('Verified and Paid Members', member, membership.referral_code)
    parent_member = membership.parent.try(:member)
    return unless parent_member.try(:banking_detail)
    Mailchimp.delete_from_list('Successful Referrers', parent_member.email)
    Mailchimp.subscribe_to_list('Successful Referrers', parent_member, nil, "#{member.first_name} #{member.last_name}")
  end

  def change_avatar
    member = self.membership.member
    begin
      file = File.open(member.avatar.copy.path, &:read)
    rescue
      file = nil
    end
    return unless file
    File.open(member.avatar.path, 'w') { |origin_file| origin_file.write file }
    File.delete(member.avatar.copy.path)
  end
end
