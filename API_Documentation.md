Varsity Vibe API:

The API is versioned and the current stable version is v1

Login:
------

`POST http://178.62.25.227/api/v1/members/login`

Parameters:
- email (String, regular email address)
- id_number (String, student number or SA ID Number)

Signup:
------

`POST http://178.62.25.227/api/v1/members/signup`

Parameters:
- email (String, regular email address)
- id_number (String, student number or SA ID Number)
- full_name (String)
- club_id (Integer, the club they're joining on Varsity Vibe)
- referral_code (String, the referral code of the person inviting you)
- gender (String, male, female, other)
- verify_now (Boolean, whether or not to immediately send the verification email)

Verification:
------

The account verification check needs to happen straight after a user signs up or logs in before
they can get through to the main application. Every member must verify their email address.

`GET http://178.62.25.227/api/v1/members/is_verified`

Parameters:
- api_key (String, the key that you received from the user account on signup or login)

Checking if an email address is already registered:
------

`GET http://178.62.25.227/api/v1/members/email_exists?email=me@me.com`

Parameters:
- email (String, a personal email address)

Checking membership payment status:
------

A student account gets a membership to a 'club' by default that's marked as unpaid until they pay for it.
In order to check if the membership is paid, you make a GET request to:

`GET http://178.62.25.227/api/v1/members/is_paid`

Parameters:
- api_key (String, the key that you received from the user account on signup or login)
- club_id (Integer, the ID of the club you want to check membership against)

Deals:
------

Each deal belongs to a business and can be available at many branches of a business, each with their own
geographic location. A deal also has one main image and many other images that can be used

`GET http://178.62.25.227/api/v1/deals?city=[city_id]&last_id=[last_id]&query[search_text]&limit=[record_limit]&sort=[sort]`

Parameters:
- city (Integer, the ID of the city you want to search within. This is optional)
- last_id (Integer, the ID of the last deal in the list if you wish to return the next batch of deals. This is optional)
- query (String, The search string if you wish to search within the deals. This is optional)
- sort (String, The sort must have one of following value: 'az', 'age', 'recommended', 'favourites')

If you just want to fetch all deals without any filtering, send a GET request to:

`GET http://178.62.25.227/api/v1/deals`

The response will contain a JSON object matching the structure of the 'deals' table and also
containing the city, merchant and branches references for each deal.

This API returns 15 deals at a time.

Fetching a single deal is done by fetching it using its ID.

`GET http://178.62.25.227/api/v1/deals/[id]`

If you want to add/delete deal to/from favourite deals send a GET request to:

`GET http://178.62.25.227/api/v1/deals/[id]/toggle_favourite_deal?api_key=[api_key]`

This request doesn't work without api_key

Cities:
------

The cities API returns a list of cities enabled in the platform.

`GET http://178.62.25.227/api/v1/cities`

User Profile:
------

The user profile endpoint returns a JSON array of all relevant user profile information.

`GET http://178.62.25.227/api/v1/members/profile`

Parameters:
- api_key (String, the key obtained during sign up or sign in)

To update the user profile, you can submit a post request to the following endpoint containing any of the available
user profile fields.

`POST http://178.62.25.227/api/v1/members/update_profile`

Parameters:
- first_name (String)
- last_name (String)
- email (String)
- id_number (String, student number or RSA ID Number)
- date_of_birth (Date)
- gender (Date)
- study_region (String)
- postal_address (String)
- postal_city (String)
- postal_province (String)
- postal_code (String)
- postal_suburb (String)
- postal_suburb (String)
- university_id (Integer)

Invite:
------

The invite endpoint simply constructs an email with the correct referral codes and sends it to the person you are inviting.

`POST http://178.62.25.227/api/v1/invites`

Parameters:
- name (String, The name of the friend you are inviting)
- email (String, The email address of the friend you are inviting)
- api_key (String, Your API key)

Activating by Agent:
--------------------

The activating by agent endpoint activate membership by referral code.

`POST http://178.62.25.227/api/v1/memberships/ref_code_check`

Parameters:
- api_key (String, Agent API key)
- referral_code (String, Member's referral code)

Universities:
------

User accounts are tied to universities, so this endpoint returns a simple list of universities:

`GET http://178.62.25.227/api/v1/universities`


Refer friends:
------

The refer friends endpoint returns a JSON data with the calculated values of total referrals, paid and unpaid.

`GET http://178.62.25.227/api/v1/members/refer_friends`

Parameters:
- api_key (String, Your API key)


Return Types:
------

The API strictly returns a JSON payload with an explicit status code embedded,
an example would be:

```
render json: {
  :status => 200,
  :member => @member
}
```

Status Codes:
200 - Success
404 - Resource could not be found
401 - Unauthorized
500 - Error

If the 404 and 500 status codes are present, the JSON payload will also contain
an *errors* array field with the server error messages.

Compilation:
------

rake assets:precompile RAILS_ENV=production
