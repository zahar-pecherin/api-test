# Varsity Vibe 2


### Steps to install locally

`bundle install`
`npm install -g bower`
`bower install`
`rails db:create`
`rails db:migrate`
`rails db:seed`

### Create test data
`rake job:create_test_data`