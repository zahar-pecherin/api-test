
namespace :job do
  task check_expire_date: :environment do
    puts "Started #{Time.now}"
    memberships = Membership.where('expires IS NOT NULL AND expires <= ? AND status = TRUE', Date.current)
    puts "Count found memberships: #{memberships.count}"
    @grey_avatar = true
    memberships.each do |membership|
      membership.status = false
      membership.save
      member = membership.member
      unless member.membership_paid?
        if member.avatar.path
          file = File.open(member.avatar.path)
          member.avatar = file
          member.save
        end
      end
      puts "Membership with #{membership.id} was canged"
    end
    puts "Ended in #{Time.now}"
  end

  task indexes_tables: :environment do
    [Membership, Collection, City, University, Merchant, Deal].each do |klass|
      puts "Import #{klass}"
      klass.index.delete
      klass.create_elasticsearch_index
      index = Tire.index klass.table_name
      index.mapping klass.table_name, properties: klass.mapping
      klass.tire.import
    end
  end

  task create_test_data: :environment do
    city_ids = City.all.pluck(:id)

    puts 'Creating - Universities'
    15.times do
      University.create!(name: Faker::University.name,
                        city_id: city_ids.sample,
                        email_slug: "@#{Faker::Internet.domain_name}")
    end

    puts 'Creating - Merchants'

    30.times do
      Merchant.create!(
          name: Faker::Company.name,
          deal_description: Faker::Lorem.sentence,
          phone: Faker::PhoneNumber.phone_number,
          email: Faker::Internet.email,
          url: Faker::Internet.url(Faker::Internet.domain_name, nil),
          terms: Faker::Lorem.paragraph
      )
    end

    puts 'Creating - Branches'

    merchant_ids = Merchant.all.pluck(:id)

    170.times do
      Branch.create!(
          merchant_id: merchant_ids.sample,
          name: Faker::Company.name,
          address: Faker::Address.street_address,
          phone: Faker::PhoneNumber.phone_number,
          email: Faker::Internet.email,
          city_id: city_ids.sample,
          lng: Faker::Address.longitude,
          lat: Faker::Address.latitude
      )
    end

    puts 'Creating - Deals'

    250.times do
      merchant = Merchant.all.sample
      deal = merchant.deals.create!(
          merchant_id: merchant.id,
          title: Faker::Lorem.sentence,
          blurb: Faker::Lorem.sentence,
          deal: Faker::Lorem.paragraph,
          terms: Faker::Lorem.paragraph,
          seo_description: Faker::Lorem.paragraph,
          seo_keyword: Faker::Lorem.words(rand(20)).join(', '),
          url: Faker::Internet.url(Faker::Internet.domain_name, nil),
          instagram: Faker::Internet.url('instagram.com'),
          facebook: Faker::Internet.url('facebook.com'),
          pinterest: Faker::Internet.url('pinterest.com'),
          twitter: Faker::Internet.url('twitter.com'),
          published: [true, false].sample,
          in_store: [true, false].sample,
          online: [true, false].sample,
          recommended: false
      )
      merchant.branches.limit(5).each do |branch|
        deal.branches << branch
      end
    end

    puts 'Creating - Members with other info'

    100.times do
      city = City.all.sample
      university = University.all.sample
      member = Member.create!(
          first_name: Faker::Name.first_name,
          last_name: Faker::Name.last_name,
          email: Faker::Internet.email,
          password: Digest::SHA1.hexdigest(Faker::Internet.password),
          study_region: city.name,
          postal_address: Faker::Address.street_address,
          postal_city: city.name,
          postal_code: Faker::Address.zip,
          mobile_number: Faker::PhoneNumber.phone_number,
          gender: %w(Male Female).sample,
          verified: false,
          university_id: university.id,
          city_id: city.id,
          student_email: "#{Faker::Internet.user_name}#{university.email_slug}"
      )

      member.membership.update_attribute(:signup_source, %w(Website Mobile).sample)

      if [true, false].sample
        member.create_banking_detail(
            account_name: member.name,
            bank_name: "#{Faker::Lorem.word.capitalize} Bank",
            account_number: Faker::Number.number(16),
            branch_code: Faker::Number.number(6)
        )
      end

      if [true, false].sample
        payment = member.membership.create_payment(
            reference_number: Faker::Number.number(7),
            payment_source: ['PayFast', 'Cash', 'Vibe Agent'].sample,
            amount_paid: '200',
            date_paid: Faker::Date.between(1.year.ago, Date.today)
        )
        member.update_attribute(:verified, true)
        member.membership.update_attributes(paid: true, status: true, expires: (payment.date_paid + 1.year), payments_id: payment.id)
      end
    end

    Membership.all.sample(70).each do |membership|
      membership.update_attribute(:referred_by, Membership.all.sample.id)
    end

    agents = Member.all.sample(5)

    agents.each do |member|
      member.update_attribute(:is_agent, true)
    end

    Payment.where(payment_source: 'Vibe Agent').each do |payment|
      agent = agents.sample
      payment.update(notes: "Collected via #{agent.email}")
      agent.collections.create!(member_id: payment.membership.member.id, amount_paid: '200', club_id: 1)
    end
  end
end
