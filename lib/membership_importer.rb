class MembershipImporter < ActiveImporter::Base
  imports Member

  column 'ID', :id_number
  column 'First name', :first_name
  column 'Last name', :last_name
  column 'Email', :email
  column 'Mobile number', :mobile_number
  column 'Date of birth', :date_of_birth
  column 'Gender', :gender
  column 'Study region', :study_region
  column 'Postal address', :postal_address
  column 'Suburb', :postal_suburb
  column 'City', :postal_city
  column 'Postal code', :postal_code, &:to_s

  skip_rows_if { row['ID'].blank? }

  fetch_model do
    Member.includes(:clubs).where(id_number: row['ID']).first_or_initialize
  end

  on :import_started do
    @rows_processed = @rows_imported = @rows_skipped = 0
    @errors = @summary = ''
    @club = Club.find(params[:club_id])

    if params[:referred_by].present?
      referred_by_membership = Membership.find_by_referral_code(params[:referred_by])
      @referred_by_id = referred_by_membership.id unless referred_by_membership.nil?
    end
  end

  on :row_processing do
    model.clubs << @club
    # if model.id.nil?
    #   model.clubs << @club
    # else
    #   membership = Membership.where(member_id: model.id, club_id: @club.id).first_or_initialize
    #   if membership.nil?
    #     Membership.new
    #     Membership.member = model
    #     Membership.club = @club
    #     Membership.save
    #   end
    # end
  end

  on :row_processed do
    @rows_processed += 1
  end

  on :row_success do
    if model.id && (!params[:referred_by].nil? || params[:paid] == 'Yes')
      membership = Membership.includes(:club, :member).individual(params[:club_id], model.id)

      if membership.id
        membership.referred_by = @referred_by_id if params[:referred_by]

        if params[:paid] == 'Yes'
          membership.paid = true
          membership.status = true

          payment = Payment.new
          payment.reference_number = 'BULK IMPORT'
          payment.payment_source = 'Import'
          payment.membership_id = membership.id
          payment.save
        end

        membership.save

        MembershipMailer.signup_email(membership.member, membership.club, membership).deliver_later
      end
    end
    @rows_imported += 1
  end

  on :row_error do |exception|
    @errors += "Error on row #{row_index} - #{exception.message}\n"
  end

  on :row_skipped do
    @rows_skipped += 1
  end

  on :import_finished do
    @summary += "#{@rows_processed} row(s) processed\n"
    @summary += "#{@rows_imported} row(s) imported successfully\n"
    @summary += "#{@rows_skipped} blank row(s) skipped\n" if @rows_skipped > 0
    @summary += @errors
  end

  on :import_failed do |exception|
    @summary = "Fatal error while importing data: #{exception.message}"
  end

  def self.summary
    @summary
  end
end
